package editor;

import java.util.ArrayList;
import java.util.List;

import net.sourceforge.texlipse.editor.ColorManager;
import net.sourceforge.texlipse.editor.scanner.TexMathScanner;
import net.sourceforge.texlipse.editor.scanner.TexSpecialCharRule;
import net.sourceforge.texlipse.editor.scanner.WhitespaceDetector;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
//import org.eclipse.jface.text.rules.MultiLineRule;
//import org.eclipse.jface.text.rules.NumberRule;
//import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;


public class SEMathScanner extends TexMathScanner {

	/**
     * A default constructor. 
     * @param manager
     */
    public SEMathScanner(ColorManager manager) {
    	super(manager);
        IToken defaultToken = new Token(
                new TextAttribute(
                        manager.getColor(ColorManager.EQUATION),
                        null,
                        manager.getStyle(ColorManager.EQUATION_STYLE)));

        IToken commentToken = new Token(
                new TextAttribute(
                        manager.getColor(ColorManager.COMMENT),
                        null,
                        manager.getStyle(ColorManager.COMMENT_STYLE)));

        //Commands are colored in math color with command styles 
        IToken commandToken = new Token(
                new TextAttribute(
                        manager.getColor(ColorManager.EQUATION),
                        null,
                        manager.getStyle(ColorManager.COMMAND_STYLE)));
        // A token that defines how to color special characters (\_, \&, \~ ...)
        IToken specialCharToken = new Token(new TextAttribute(manager
                .getColor(ColorManager.TEX_SPECIAL),
                null,
                manager.getStyle(ColorManager.TEX_SPECIAL_STYLE)));
        
        List<IRule> rules = new ArrayList<IRule>();
        
        rules.add(new WhitespaceRule(new WhitespaceDetector()));
        rules.add(new TexSpecialCharRule(specialCharToken));
        //rules.add(new SingleLineRule("\\%", " ", specialCharToken));
        rules.add(new EndOfLineRule("%", commentToken));
        /*rules.add(new TexEnvironmentRule("comment", commentToken));
        rules.add(new SingleLineRule("\\[", " ", defaultToken));
        rules.add(new SingleLineRule("\\]", " ", defaultToken));
        rules.add(new SingleLineRule("\\(", " ", defaultToken));
        rules.add(new SingleLineRule("\\)", " ", defaultToken));*/
        rules.add(new WordRule(new SEWord(), commandToken));
        
        IRule[] result = new IRule[rules.size()];
        rules.toArray(result);
        setRules(result);		
    }
}