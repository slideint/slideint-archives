package editor;

import java.util.ArrayList;
import java.util.List;

import net.sourceforge.texlipse.editor.ColorManager;
import net.sourceforge.texlipse.editor.scanner.TexArgScanner;
import net.sourceforge.texlipse.editor.scanner.WhitespaceDetector;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
//import org.eclipse.jface.text.rules.MultiLineRule;
//import org.eclipse.jface.text.rules.NumberRule;
//import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;

public class SEArgScanner extends TexArgScanner {

/**
     * A default constructor.
     * @param manager
     */
    public SEArgScanner(ColorManager manager) {
    	super(manager);
        IToken commentToken = new Token(new TextAttribute(manager
                .getColor(ColorManager.COMMENT),
                null,
                manager.getStyle(ColorManager.COMMENT_STYLE)));

        //Commands are colored in argument color with command styles 
        IToken commandToken = new Token(
                new TextAttribute(
                        manager.getColor(ColorManager.CURLY_BRACKETS),
                        null,
                        manager.getStyle(ColorManager.COMMAND_STYLE)));

        List<IRule> rules = new ArrayList<IRule>();
        rules.add(new EndOfLineRule("%", commentToken, '\\'));
        rules.add(new WhitespaceRule(new WhitespaceDetector()));
        rules.add(new WordRule(new SEWord(), commandToken));

        IRule[] result = new IRule[rules.size()];
        rules.toArray(result);
        setRules(result);
    }
}