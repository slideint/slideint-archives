package editor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.NumberRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;

import net.sourceforge.texlipse.editor.ColorManager;
import net.sourceforge.texlipse.editor.scanner.TexScanner;
import net.sourceforge.texlipse.editor.scanner.TexSpecialCharRule;
import net.sourceforge.texlipse.editor.scanner.WhitespaceDetector;

public class SEScanner extends TexScanner {


    public SEScanner(ColorManager manager) {
    	super(manager);
        // A token that defines how to color numbers
        IToken numberToken = new Token(new TextAttribute(manager
                .getColor(ColorManager.TEX_NUMBER),
                null,
                manager.getStyle(ColorManager.TEX_NUMBER_STYLE)));

        // A token that defines how to color command words (\command_word)
        IToken commandToken = new Token(new TextAttribute(manager
                .getColor(ColorManager.COMMAND),
                null,
                manager.getStyle(ColorManager.COMMAND_STYLE)));

        IToken commentToken = new Token(new TextAttribute(manager
                .getColor(ColorManager.COMMENT),
                null,
                manager.getStyle(ColorManager.COMMENT_STYLE)));

        // A token that defines how to color special characters (\_, \&, \~ ...)
        IToken specialCharToken = new Token(new TextAttribute(manager
                .getColor(ColorManager.TEX_SPECIAL),
                null,
                manager.getStyle(ColorManager.TEX_SPECIAL_STYLE)));
        
        List<IRule> rules = new ArrayList<IRule>();
        rules.add(new TexSpecialCharRule(specialCharToken));
        rules.add(new WordRule(new SEWord(), commandToken));
        rules.add(new NumberRule(numberToken));
        rules.add(new EndOfLineRule("%", commentToken, '\\'));
        rules.add(new WhitespaceRule(new WhitespaceDetector()));
        
        IRule[] result = new IRule[rules.size()];
        rules.toArray(result);
        setRules(result);
    }
}