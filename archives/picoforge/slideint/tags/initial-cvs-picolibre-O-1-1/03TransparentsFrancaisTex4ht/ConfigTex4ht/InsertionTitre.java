/**
 * Title : InsertionTitreHtml
 * Description : Class for inserting HTML text just after <body>
 *               Be careful! It is assumed that <body> is alone on a line.
 * Copyright:    Copyright (c) 2003
 * Company:      INT, Evry, France
 * @author Denis Conan
 */  

import java.io.*;
import java.util.*;

public class InsertionTitreHtml {

    /**
     * File to write debug info in case...
     */
    private static PrintWriter fileDebug = null;

    /**
     * File to write the resulting HTML text
     */
    private static PrintWriter fileResult = null;

    /**
     * File for reading HTML text to insert
     */
    private static LectureFichierResultats fileInsert;

    /**
     * File for reading HTML text to complement
     */
    private static LectureFichierResultats fileRC;

    /**
     * Vector of HTML entries to insert
     */
    private static Vector insert = new Vector();

    /**
     * Reads HTML file to insert.
     * @param fileName name of the file.
     */
    public static void readInsertFile(String fileName) {
	String line = null;

	try {
	    fileInsert = new LectureFichierResultats(new FileReader(fileName));
	    // Read line by line
	    while (fileInsert.newLine()) {
		// Insert all the line
		line = fileInsert.readRemainingString();
		insert.addElement(line);
		fileDebug.println("Insert Line: " + line);
	    }
	    fileInsert.close();
	    fileDebug.println("Size of the insert Vector: " + insert.size());
	} catch (Exception ex) {
	    fileDebug.println(ex);
	    ex.printStackTrace();
	    System.err.println(fileName);
	}
    }

    /**
     * Reads HTML file to complement and write.
     * Be careful! It is assumed that <body> is alone on a line.
     * @param fileNameRC name of the file to read and complement.
     * @param fileResult file to write.
     */
    public static void readComplementWriteFile(String fileNameRC,
					       FileWriter fileResult) {
	String line = null;
	try {
	    fileRC = new LectureFichierResultats(new FileReader(fileNameRC));
	    // Read and write line by line up to the line containing <body>
	    while (fileRC.newLine()) {
		// Read and write the whole line
		line = fileRC.readRemainingString();
		// test if equals <body>
		if (line.equals("<body>")) {
		    break;
		} else {
		    fileResult.println(line);
		    fileDebug.println("Result Line before insertion: " + line);
		}
	    }
	    // Write the text to insert
	    fileDebug.println("Size of the insert Vector: " + insert.size());
	    for (int i = 0; i < insert.size(); i++) {
		line = (String) insert.elementAt(i);
		fileResult.println(line);
		fileDebug.println("Result Line during insertion: " + line);
	    }
	    // Read and write line by line up to the end of the file
	    while (fileRC.newLine()) {
		// Read and write the whole line
		line = fileRC.readRemainingString();
		fileResult.println(line);
		fileDebug.println("Result Line after insertion: " + line);
	    }
	    fileResult.close();
	} catch (Exception ex) {
	    fileDebug.println(ex);
	    ex.printStackTrace();
	    System.err.println(fileNameRC + " " +  fileNameW);
	}
    }

    public static void insertTitle(String args[]) {
	// open tmp file to write debug info
	try {
	    fileDebug = new PrintWriter
		(new FileWriter (new File("insertion.log")), true);
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
	// Read the HTML text to insert
	readInsertFile(args[0]);
	// Read the HTML file to complement and write the resulting file
	readComplementWriteFile(args[1], args[2]);
	fileDebug.close();
    }
}
