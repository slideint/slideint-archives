/**
 * Title : LectureFichierResultats
 * Description : Class for reading formated data 
 * inside a file
 * Copyright:    Copyright (c) 2002,2003
 * Company:      INT, Evry, France
 * @author Olivier Villin
 * @author Erik Putrycz
 * @author Denis Conan
 */  
import java.io.*;

public class LectureFichierResultats extends java.io.BufferedReader {
    static boolean debug=false; 
    private String linebuffer="";
    public static String separator = " ";

    /**
     * Constructor of the class
     * @param in the file reader to read
     */  
    public LectureFichierResultats(Reader in) {
	super(in);
    }
    
    /**
     * Read an integer from the file given to the constructor 
     */
    public int readInt() throws IOException, NumberFormatException {
	String strint="";
	try {
	    if (linebuffer.equals("")) linebuffer=readLine();
	    int spc_pos=linebuffer.indexOf(separator);
	     if (debug) {
		System.out.println(spc_pos);
	    }
	  
	    if (spc_pos==-1){
		strint=linebuffer;
		 if (debug) {
		System.out.println(strint);
	    }
		linebuffer="";
	    } 
	    else {
		strint=linebuffer.substring(0,spc_pos);
		linebuffer=linebuffer.substring(spc_pos+1);
	    }
	    if (debug) {
		System.out.println(strint);
	    }
	    return Integer.parseInt(strint);
	}
	catch (Exception ex) {
	    ex.printStackTrace();
	}
	finally {
	    return Integer.parseInt(strint);
	}
    }
    
    /**
     * Read a double from the file given to the constructor 
     */
    public double readDouble() throws IOException, NumberFormatException {
	String strint="";
	try {
	    if (linebuffer.equals("")) linebuffer=readLine();
	    int spc_pos=linebuffer.indexOf(separator);
  	    if (spc_pos==-1){
		strint=linebuffer;
		linebuffer="";
	    } 
	    else {
		strint=linebuffer.substring(0,spc_pos);
		linebuffer=linebuffer.substring(spc_pos+1);
	    }
	    return Double.parseDouble(strint);
	}
	catch (Exception ex) {
	    ex.printStackTrace();
	}
	finally {
	    return Double.parseDouble(strint);
	}
    }  
   
    /**
     * Read a float from a file
     */  
    public float readFloat() throws IOException, NumberFormatException {
	String strint="";
	try {
	    if (linebuffer.equals("")) linebuffer=readLine();
	    int spc_pos=linebuffer.indexOf(separator);
	    if (spc_pos==-1){
		strint=linebuffer;
		linebuffer="";
	    } 
	    else {
		strint=linebuffer.substring(0,spc_pos);
		linebuffer=linebuffer.substring(spc_pos+1);
	    }
	    return Float.parseFloat(strint);
	}
	catch (Exception ex) {
	    ex.printStackTrace();
	}
	finally {
	    return Float.parseFloat(strint);
	}
    }  

    /**
     * Read a short from a file
     */
    public short readShort() throws IOException, NumberFormatException {
	String strint="";
	try {
	    if (linebuffer.equals("")) linebuffer=readLine();
	    int spc_pos=linebuffer.indexOf(separator);
	    if (spc_pos==-1){
		strint=linebuffer;
		linebuffer="";
	    } 
	    else {
		strint=linebuffer.substring(0,spc_pos);
		linebuffer=linebuffer.substring(spc_pos+1);
	    }
	    return Short.parseShort(strint);
	}
	catch (Exception ex) {
	    ex.printStackTrace();
	}
	finally {
	    return Short.parseShort(strint);
	}
    }  
    
    /**
     * Read a string  
     */
    public String readString() throws IOException, NumberFormatException {
	String strint="";
	try {
	    if (linebuffer.equals("")) linebuffer=readLine();
	    int spc_pos=linebuffer.indexOf(separator);
	    
	    if (spc_pos==-1)
		{
		    strint=linebuffer;
		    linebuffer="";
		} 
	    else {
		strint=linebuffer.substring(0,spc_pos);
		linebuffer=linebuffer.substring(spc_pos+1);
	    }
	    return strint;
	}
	catch (Exception ex) {
	    ex.printStackTrace();
	}
	finally {
	    return strint;
	}
    } 
    
    /**
     * Read the remaining of the line as a string  
     */
    public String readRemainingString()
	throws IOException, NumberFormatException {
	return linebuffer;
    } 
   
     /**
     * Read a Boolean from the file given to the constructor 
     */
    public boolean readBoolean() throws IOException, NumberFormatException {
	String strint="";
	Boolean ret= new Boolean(true);
	try {
	    if (linebuffer.equals("")) linebuffer=readLine();
	    int spc_pos=linebuffer.indexOf(separator);
  	    if (spc_pos==-1){
		strint=linebuffer;
		linebuffer="";
	    } 
	    else {
		strint=linebuffer.substring(0,spc_pos);
		linebuffer=linebuffer.substring(spc_pos+1);
	    }
	    if (debug) {
		System.out.println("DebugValue : "+strint );
	    }
	    ret= new Boolean(strint);
	  
	}
	catch (Exception ex) {
	    ex.printStackTrace();
	}
	return ret.booleanValue();
    }  
   
    /**
     * Ignore end of line
     */
    public void ignoreRemainingOfLine() {
	linebuffer = "";
	// next read will provoke a readLine
    }  
   
    /**
     * Test if a new line exists, if so, readLine
     */
    public boolean newLine() {
	try {
	    linebuffer = readLine();
	    if (linebuffer == null) {
		return false;
	    } else {
		return true;
	    }
	} catch (Exception ex) {
	    linebuffer = "";
	    return false;
	}
    }  
}
