/**
 * Title : GenerationMenu
 * Description : Class for creating Menu from tochtml.tex file
 * Copyright:    Copyright (c) 2004
 * Company:      INT, Evry, France
 * @author Denis Conan
 */  

import java.io.*;
import java.util.*;

public class GenerationMenu {

    /**
     * File to write debug info in case...
     */
    private static PrintWriter fileDebug = null;

    /**
     * File to write the resulting HTML code
     */
    private static PrintWriter fileResult = null;

    /**
     * File for reading toc file
     */
    private static LectureFichierResultats fileR;

    /**
     * File for inserting HTML code before menu
     */
    private static LectureFichierResultats fileInsertBefore;

    /**
     * File for inserting HTML code at the end of the menu when < 10 entries
     */
    private static LectureFichierResultats fileInsertFill;

    /**
     * File for inserting HTML code after menu
     */
    private static LectureFichierResultats fileInsertAfter;

    /**
     * File for inserting HTML code at the end
     */
    private static LectureFichierResultats fileInsertEnd;

    /**
     * File for reading HTML content file
     */
    private static LectureFichierResultats fileContent;

    /**
     * Vector of toc entries
     */
    private static Vector toc = null;

    /**
     * A toc entry is
     * 0: a class (section, subsection, subsubsection),
     * 1: a section number,
     * 2: a subsection number,
     * 3: a subsubsection number,
     * 4: a title,
     * 5: a file name,
     * 6: a String indicating whether the entry is displayed
     */
    private static String[] tocEntry = null;

    /**
     * Reads LaTeX tochtml file.
     * @param fileName name of the LaTeX tochtml file.
     */
    public static void readTocFile(String fileName) {
	toc = new Vector();
	try {
	    fileR = new LectureFichierResultats(new FileReader(fileName));
	    while (fileR.newLine()) {
		// Create new toc entry (last String for displaying)
		tocEntry = new String[7];
		tocEntry[0] = fileR.readRemainingString();
		fileR.newLine();
		tocEntry[1] = fileR.readRemainingString();
		fileR.newLine();
		tocEntry[2] = fileR.readRemainingString();
		fileR.newLine();
		tocEntry[3] = fileR.readRemainingString();
		fileR.newLine();
		tocEntry[4] = fileR.readRemainingString();
		fileR.newLine();
		tocEntry[5] = fileR.readRemainingString();
		tocEntry[6] = "false";
		toc.addElement(tocEntry);
	    }
	    fileR.close();
	} catch (Exception ex) {
	    fileDebug.println(ex);
	    ex.printStackTrace();
	    System.err.println(fileName);
	}
    }

   /**
     * Writes the item parts of the menu.
     * @param inputDir directory of the input files
     * @param output directory of the result files
     * @param fileBefore file name of the HTML code to insert before the menu
     * @param fileFill file name of the HTML code to fill the menu
     * @param fileAfter file name of the HTML code to insert after the menu
     * @param fileEnd file name of the HTML code to insert at the end
     */
    public static void writeHTMLFiles(String inputDir, String outputDir,
				      String fileBefore, String fileFill,
				      String fileAfter, String fileEnd) {
	int i = 0;
	int j = 0;
	int k = 0;
	int nbDisplayed = 0;
	String[] menuTocEntry;
	String[] printTocEntry;
	String line = null;
	fileDebug.println("===== # toc entries: " + toc.size());
	try {
	    // Generate one file per toc entry
	    for (i = 0; i < toc.size(); i++) {
		tocEntry = (String[]) toc.elementAt(i);
		fileDebug.println("===== # toc entry " + i + " : " +
				  tocEntry[0] + " " +
				  tocEntry[1] + " " +
				  tocEntry[2] + " " +
				  tocEntry[3] + " " +
				  tocEntry[4] + " " +
				  tocEntry[5]);
		// create and open the file
		fileResult = new PrintWriter
		    (new FileWriter (new File(outputDir + tocEntry[5])), true);
		nbDisplayed = 0;
		for (j = 0; j < toc.size(); j++) {
		    menuTocEntry = (String[]) toc.elementAt(j);
		    // by default, not displayed
		    menuTocEntry[6] = "false";
		    // test if to be displayed
		    if (menuTocEntry[0].equals("section")
			|| menuTocEntry[0].equals("SECTION")) {
			// always display sections
			menuTocEntry[6] = "true";
			nbDisplayed++;
		    } else if ((menuTocEntry[0].equals("subsection")
				|| menuTocEntry[0].equals("SUBSECTION"))
			       && (menuTocEntry[1].equals(tocEntry[1]))) {
			// display subsections in the selected section
			menuTocEntry[6] = "true";
			nbDisplayed++;
		    } else if ((menuTocEntry[0] .equals("subsubsection"))
			       && (menuTocEntry[1].equals(tocEntry[1]))
			       && (menuTocEntry[2].equals(tocEntry[2]))) {
			// display subsubsections in the selected subsection
			menuTocEntry[6] = "true";
			nbDisplayed++;
		    }
		    fileDebug.println(menuTocEntry[0] + " " +
				      menuTocEntry[1] + " " +
				      menuTocEntry[2] + " " +
				      menuTocEntry[3] + " " +
				      menuTocEntry[4] + " " +
				      menuTocEntry[5] + " " +
				      menuTocEntry[6]);
		}
		// read the beginning of the file up to </head>
		// be careful! It is assumed that </head> is alone on a line.
		fileContent =
		    new LectureFichierResultats(new FileReader(inputDir +
							       tocEntry[5]));
		// read and write line by line up to the line containing <body>
		while (fileContent.newLine()) {
		    // read and write the whole line
		    line = fileContent.readRemainingString();
		    // test if equals <body>
		    if (line.equals("</head>")) {
			break;
		    } else {
			fileResult.println(line);
		    }
		}
		// write the enclosing tables
		// i.e., the HTML text to insert before menu
		fileDebug.println("=====Before menu file");
		// Open before menu HTML code file
		fileInsertBefore = new LectureFichierResultats(new FileReader(fileBefore));
		while (fileInsertBefore.newLine()) {
		    // read and write the whole line
		    line = fileInsertBefore.readRemainingString();
		    fileResult.println(line);
		}
		fileInsertBefore.close();
		// write menu
		for (k = 0; k < toc.size(); k++) {
		    printTocEntry = (String[]) toc.elementAt(k);
		    if (printTocEntry[6].equals("true")) {
			fileResult.print("<tr>\n" +
					 "  <td colspan=\"3\"></td>\n" +
					 "</tr>\n" +
					 "<tr>\n" +
					 "  <td width=\"1\" class=\"");
			if (k == i) {
			    fileResult.print("selectionne");
			} else {
			    fileResult.print(printTocEntry[0].toLowerCase());
			}
			fileResult.print("\"><img src=\"./Images/espaceur.gif\" width=\"1\" height=\"20\" alt=\"\"></td>\n" +
					 "  <td style=\"vertical-align:top;\" width=\"12\" class=\"");
			if (k == i) {
			    fileResult.print("selectionne");
			} else {
			    fileResult.print(printTocEntry[0].toLowerCase());
			}
			fileResult.print("\"><img src=\"./Images/carre.gif\" width=\"12\" height=\"11\" alt=\"\"></td>\n" +
					 "  <td class=\"");
			if (k == i) {
			    fileResult.print("selectionne");
			} else {
			    fileResult.print(printTocEntry[0].toLowerCase());
			}
			fileResult.print("\">&nbsp;");
			
			if (k == i) {
			    fileResult.print(printTocEntry[4]);
			} else {
			    fileResult.print("<a href=\"" +
					     printTocEntry[5] +
					     "\" class=\"" +
					     printTocEntry[0].toLowerCase() +
					     "\">" +
					     printTocEntry[4] +
					     "</a>");
			}
			fileResult.println("</td>\n" +
					   "</tr>");
		    }
		}
		// add empty entries if not enough
		if (nbDisplayed < 10) {
		    fileDebug.println("=====Fill file");
		    fileInsertFill = new LectureFichierResultats(new FileReader(fileFill));
		    while (fileInsertFill.newLine()) {
			// read and write the whole line
			line = fileInsertFill.readRemainingString();
			fileResult.println(line);
		    }
		    fileInsertFill.close();
		}
		// write the end of enclosing tables
		// i.e., the HTML text to insert after menu
		fileDebug.println("=====After menu file");
		fileInsertAfter = new LectureFichierResultats(new FileReader(fileAfter));
		while (fileInsertAfter.newLine()) {
		    // read and write the whole line
		    line = fileInsertAfter.readRemainingString();
		    fileResult.println(line);
		}
		fileInsertAfter.close();
		// read the end of the file up to </body>
		// be careful! It is assumed that </body> is alone on a line
		fileDebug.println("=====End of file");
		while (fileContent.newLine()) {
		    // read and write the whole line
		    line = fileContent.readRemainingString();
		    // copy the line except if <body>
		    // be careful! It is assumed that <body> is alone on a line
		    if (line.equals("<body>")) {
			continue;
		    } else if (line.equals("</body>")) {
			// test if equals </body>
			break;
		    } else {
			fileResult.println(line);
		    }
		}
		// write the end of HTML file
		fileDebug.println("=====End menu file");
		fileInsertEnd = new LectureFichierResultats(new FileReader(fileEnd));
		while (fileInsertEnd.newLine()) {
		    // read and write the whole line
		    line = fileInsertEnd.readRemainingString();
		    fileResult.println(line);
		}
		fileInsertEnd.close();
		fileResult.close();
	    }
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }

    public static void main(String args[]) {
	// open tmp file to write debug info
	try {
	    fileDebug = new PrintWriter
		(new FileWriter (new File(args[0] + ".mtmp")), true);
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
	// Read the results file
	readTocFile(args[0]);
	// Write the HTML items
	writeHTMLFiles(args[1], args[2], args[3], args[4], args[5], args[6]);
	try {
	    fileDebug.close();
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }
}
