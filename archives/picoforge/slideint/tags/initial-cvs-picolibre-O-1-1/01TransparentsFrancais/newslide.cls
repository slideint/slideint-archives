%%
%% This is file `newslide.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% newslide.dtx  (with options: `class')
%% 
%% IMPORTANT NOTICE:
%% 
%% For the copyright see the source file.
%% 
%% Any modified versions of this file must be renamed
%% with new filenames distinct from newslide.cls.
%% 
%% For distribution of the original source see the terms
%% for copying and modification in the file newslide.dtx.
%% 
%% This generated file may be distributed as long as the
%% original source files, as listed above, are part of the
%% same distribution. (The sources need not necessarily be
%% in the same archive or directory.)
\def\filedate{2006/11/04}
%% newslide.dtx
%% Copyright (c) 2003--2006 Denis Conan
\NeedsTeXFormat{LaTeX2e}[1996/12/01]
\ProvidesFile{newslide.dtx}[2006/11/04]
\NeedsTeXFormat{LaTeX2e}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{seminar}}
\ProcessOptions
\LoadClass{seminar}
\RequirePackage{semrot}
\RequirePackage{sem-page}
\input{seminar.bug}
\input{seminar.bg2}
\RequirePackage{ifthen}
\RequirePackage{semcolor}
\RequirePackage{semlayer}
\renewcommand{\section}{\@startsection%
   {section}% %the name
   {1}% %the level
   {\fontdimen6\font}% %the indent
   {\fontdimen5\font minus \fontdimen5\font}% %the beforeskip
   {\fontdimen5\font minus \fontdimen5\font}% %the afterskip
   {\titlesecslidestyle}}% %the style
\renewcommand{\subsection}{\@startsection%
   {subsection}% %the name
   {2}% %the level
   {\fontdimen6\font}% %the indent
   {\fontdimen5\font minus \fontdimen5\font}% %the beforeskip
   {\fontdimen5\font minus \fontdimen5\font}% %the afterskip
   {\titlesubsecslidestyle}}% %the style
\renewcommand{\subsubsection}{\@startsection%
   {subsubsection}% %the name
   {3}% %the level
   {\fontdimen6\font}% %the indent
   {\fontdimen5\font minus \fontdimen5\font}% %the beforeskip
   {\fontdimen5\font minus \fontdimen5\font}% %the afterskip
   {\titlesubsubsecslidestyle}}% %the style
\newcommand{\titlesecslidestyle}{\large\bfseries\centering}
\newcommand{\titlesubsecslidestyle}{\large\bfseries\centering}
\newcommand{\titlesubsubsecslidestyle}{\large\bfseries\centering}
\setcounter{secnumdepth}{3} % down to subsubsections
\setcounter{tocdepth}{3} % down to subsubsections for toc
\newcounter{currentslidetocdepth}
\def\thetitlesecslide{}
\def\titlesecslide#1{\gdef\thetitlesecslide{#1}}
\def\thetitlesubsecslide{}
\def\titlesubsecslide#1{\gdef\thetitlesubsecslide{#1}}
\def\thetitlesubsubsecslide{}
\def\titlesubsubsecslide#1{\gdef\thetitlesubsubsecslide{#1}}
\def\theslidenumero{}
\def\slidenumero#1{\gdef\theslidenumero{#1}}
\newwrite\tocall
\newwrite\tocsection
\newwrite\tocsubsection
\AtEndDocument{\message{AED: newslide close toc_all}\closeout\tocall}
\newcounter{tocall}
\newcounter{tocsection}
\newcounter{tocsubsection}
\newcounter{tocsubsubsection}
\newcommand{\initsecslide}{}
\newcommand{\initsubsecslide}{}
\newcommand{\initsubsubsecslide}{}
\newcommand{\initendsecslide}{}
\newcommand{\initendsubsecslide}{}
\newcommand{\initendsubsubsecslide}{}
\ifarticle
  \def\thenote{\thepage}
\else
\fi
\newenvironment{secslide}[2][slidetitletoc]{%
   \initsecslide%
   \setcounter{currentslidetocdepth}{1}%
   \addtocounter{section}{1}%
   \setcounter{subsection}{0}%
   \setcounter{subsubsection}{0}%
   \ifarticle\addcontentsline{toc}{section}{\thesection~#2}\fi%
   \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tocall{\noindent\thesection\noexpand~ #2\noexpand\dotfill\theslide\noexpand\\}}{\write\tocall{\noindent\thesection\noexpand~ #1\noexpand\dotfill\theslide\noexpand\\}}
   \begin{slide}%
      \protect\section*{\thesection~#2}%
      \closeout\tocsection%
      \addtocounter{tocsection}{1}%
      \openout\tocsection=toc_sec_\thetocsection%
      \slidenumero{\thesection}%
      \titlesecslide{\thesection~#2}%
      \titlesubsecslide{~}\titlesubsubsecslide{~}}
   {\end{slide}}
\newenvironment{Psecslide}[2][slidetitletoc]{%
   \initsecslide%
   \addtocounter{section}{1}%
   \setcounter{subsection}{0}%
   \setcounter{subsubsection}{0}%
   \ifarticle\addcontentsline{toc}{section}{\thesection~#2}\fi%
   \setcounter{currentslidetocdepth}{1}%
   \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tocall{\noindent\thesection\noexpand~ #2\noexpand\dotfill\theslide\noexpand\\}}{\write\tocall{\noindent\thesection\noexpand~ #1\noexpand\dotfill\theslide\noexpand\\}}
   \begin{slide*}%
      \protect\section*{\thesection~#2}%
      \closeout\tocsection%
      \addtocounter{tocsection}{1}%
      \openout\tocsection=toc_sec_\thetocsection%
      \slidenumero{\thesection}%
      \titlesecslide{\thesection~#2}%
      \titlesubsecslide{~}\titlesubsubsecslide{~}}
   {\end{slide*}}
\newenvironment{secslide*}[2][uselessargument]{%
   \initsecslide%
   \setcounter{currentslidetocdepth}{1}%
   \setcounter{subsection}{0}%
   \setcounter{subsubsection}{0}%
   \ifarticle\addcontentsline{toc}{section}{#2}\fi%
   \begin{slide}%
      \protect\section*{#2}%
      \closeout\tocsection%
      \addtocounter{tocsection}{1}%
      \openout\tocsection=toc_sec_\thetocsection%
      \slidenumero{}%
      \titlesecslide{#2}%
      \titlesubsecslide{~}\titlesubsubsecslide{~}}
   {\end{slide}}
\newenvironment{Psecslide*}[2][uselessargument]{%
   \initsecslide%
   \setcounter{currentslidetocdepth}{1}%
   \setcounter{subsection}{0}%
   \setcounter{subsubsection}{0}%
   \ifarticle\addcontentsline{toc}{section}{#2}\fi%
   \begin{slide*}%
      \protect\section*{#2}%
      \closeout\tocsection%
      \addtocounter{tocsection}{1}%
      \openout\tocsection=toc_sec_\thetocsection%
      \slidenumero{}%
      \titlesecslide{#2}%
      \titlesubsecslide{~}\titlesubsubsecslide{~}}
   {\end{slide*}}
\newenvironment{subsecslide}[2][slidetitletoc]{%
   \initsubsecslide%
   \setcounter{currentslidetocdepth}{2}%
   \addtocounter{subsection}{1}%
   \setcounter{subsubsection}{0}%
   \ifarticle\addcontentsline{toc}{subsection}{\thesubsection~#2}\fi%
   \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tocsection{\noindent\thesubsection\noexpand~ #2\noexpand\dotfill\theslide\noexpand\\}}{\write\tocsection{\noindent\thesubsection\noexpand~ #1\noexpand\dotfill\theslide\noexpand\\}}
   \begin{slide}%
      \protect\subsection*{\thesubsection~#2}%
      \closeout\tocsubsection%
      \addtocounter{tocsubsection}{1}%
      \openout\tocsubsection=toc_sub_sec_\thetocsubsection%
      \slidenumero{\thesubsection}%
      \titlesubsecslide{\thesubsection~#2}\titlesubsubsecslide{~}}
   {\end{slide}}
\newenvironment{Psubsecslide}[2][slidetitletoc]{%
   \initsubsecslide%
   \setcounter{currentslidetocdepth}{2}%
   \addtocounter{subsection}{1}%
   \setcounter{subsubsection}{0}%
   \ifarticle\addcontentsline{toc}{subsection}{\thesubsection~#2}\fi%
   \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tocsection{\noindent\thesubsection\noexpand~ #2\noexpand\dotfill\theslide\noexpand\\}}{\write\tocsection{\noindent\thesubsection\noexpand~ #1\noexpand\dotfill\theslide\noexpand\\}}
   \begin{slide*}%
      \protect\subsection*{\thesubsection~#2}%
      \closeout\tocsubsection%
      \addtocounter{tocsubsection}{1}%
      \openout\tocsubsection=toc_sub_sec_\thetocsubsection%
      \slidenumero{\thesubsection}%
      \titlesubsecslide{\thesubsection~#2}\titlesubsubsecslide{~}}
   {\end{slide*}}
\newenvironment{subsecslide*}[2][uselessargument]{%
   \initsubsecslide%
   \setcounter{currentslidetocdepth}{2}%
   \setcounter{subsubsection}{0}%
   \ifarticle\addcontentsline{toc}{subsection}{#2}\fi%
   \begin{slide}%
      \protect\subsection*{#2}%
      \closeout\tocsubsection%
      \addtocounter{tocsubsection}{1}%
      \openout\tocsubsection=toc_sub_sec_\thetocsubsection%
      \slidenumero{}%
      \titlesubsecslide{#2}\titlesubsubsecslide{~}}
   {\end{slide}}
\newenvironment{Psubsecslide*}[2][uselessargument]{%
   \initsubsecslide%
   \setcounter{currentslidetocdepth}{2}%
   \setcounter{subsubsection}{0}%
   \ifarticle\addcontentsline{toc}{subsection}{#2}\fi%
   \begin{slide*}%
      \protect\subsection*{#2}%
      \closeout\tocsubsection%
      \addtocounter{tocsubsection}{1}%
      \openout\tocsubsection=toc_sub_sec_\thetocsubsection%
      \slidenumero{}%
      \titlesubsecslide{#2}\titlesubsubsecslide{~}}
   {\end{slide*}}
\newenvironment{subsubsecslide}[2][slidetitletoc]{%
   \initsubsubsecslide%
   \setcounter{currentslidetocdepth}{3}%
   \addtocounter{subsubsection}{1}%
   \ifarticle\addcontentsline{toc}{subsubsection}{\thesubsubsection~#2}\fi%
   \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tocsubsection{\noindent\thesubsubsection\noexpand~ #2\noexpand\dotfill\theslide\noexpand\\}}{\write\tocsubsection{\noindent\thesubsubsection\noexpand~ #1\noexpand\dotfill\theslide\noexpand\\}}
   \begin{slide}%
      \protect\subsubsection*{\thesubsubsection~#2}%
      \addtocounter{tocsubsubsection}{1}%
      \slidenumero{\thesubsubsection}%
      \titlesubsubsecslide{\thesubsubsection~#2}}%
   {\end{slide}}
\newenvironment{Psubsubsecslide}[2][slidetitletoc]{%
   \initsubsubsecslide%
   \setcounter{currentslidetocdepth}{3}%
   \addtocounter{subsubsection}{1}%
   \ifarticle\addcontentsline{toc}{subsubsection}{\thesubsubsection~#2}\fi%
   \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tocsubsection{\noindent\thesubsubsection\noexpand~ #2\noexpand\dotfill\theslide\noexpand\\}}{\write\tocsubsection{\noindent\thesubsubsection\noexpand~ #1\noexpand\dotfill\theslide\noexpand\\}}
   \begin{slide*}%
      \protect\subsubsection*{\thesubsubsection~#2}%
      \addtocounter{tocsubsubsection}{1}%
      \slidenumero{\thesubsubsection}%
      \titlesubsubsecslide{\thesubsubsection~#2}}%
   {\end{slide*}}
\newenvironment{subsubsecslide*}[2][uselessargument]{%
   \initsubsubsecslide%
   \setcounter{currentslidetocdepth}{3}%
   \ifarticle\addcontentsline{toc}{subsubsection}{#2}\fi%
   \begin{slide}%
      \protect\subsubsection*{#2}%
      \addtocounter{tocsubsubsection}{1}%
      \slidenumero{}%
      \titlesubsubsecslide{#2}}%
   {\end{slide}}
\newenvironment{Psubsubsecslide*}[2][uselessargument]{%
   \initsubsubsecslide%
   \setcounter{currentslidetocdepth}{3}%
   \ifarticle\addcontentsline{toc}{subsubsection}{#2}\fi%
   \begin{slide*}%
      \protect\subsubsection*{#2}%
      \addtocounter{tocsubsubsection}{1}%
      \slidenumero{}%
      \titlesubsubsecslide{#2}}%
   {\end{slide*}}
\renewcommand{\listofslides}{\tableofcontents}
\newcommand{\intocslide}{}
\newcommand{\tocslide}[4][slidetitletoc]{%
   \begin{#4}[#1]{#2}%
   \vfill%
   \ifthenelse{\equal{#3}{secslides}}%
      {\addtocounter{tocall}{1}%
       \IfFileExists{toc_\thetocall}%
  {\input{toc_\thetocall}}%
  {TOC file does not exist}%
       \openout\tocall=toc_\thetocall}%
      {}%
   \ifthenelse{\equal{#3}{subsecslides}}%
      {\IfFileExists{toc_sec_\thetocsection}%
  {\input{toc_sec_\thetocsection}}%
  {TOC file does not exist}}%
      {}%
   \ifthenelse{\equal{#3}{subsubsecslides}}%
{\IfFileExists{toc_sub_sec_\thetocsubsection}%
  {\input{toc_sub_sec_\thetocsubsection}}%
  {TOC file does not exist}}
{}%
   \vfill%
   \end{#4}%
}
\renewcommand{\theoverlay}{\theslide}
\makeatletter
\def\pst@initoverlay#1{%
\pst@Verb{%
/BeginOL {dup (all) eq exch TheOL le or {IfVisible not {Visible
/IfVisible true def} if} {IfVisible {Invisible /IfVisible false def} if}
ifelse} def
\tx@InitOL /TheOL (#1) def}}
\makeatother
\addtoslidelist{secslide}
\addtoslidelist{Psecslide}
\addtoslidelist{secslide*}
\addtoslidelist{Psecslide*}
\addtoslidelist{subsecslide}
\addtoslidelist{Psubsecslide}
\addtoslidelist{subsecslide*}
\addtoslidelist{Psubsecslide*}
\addtoslidelist{subsubsecslide}
\addtoslidelist{Psubsubsecslide}
\addtoslidelist{subsubsecslide*}
\addtoslidelist{Psubsubsecslide*}
\noxcomment
\makeatletter
\renewcommand{\label}[1]{\@bsphack\protected@write\@auxout{}%
   {\string\newlabel{#1}{{\theslidenumero}{\thepage}}}%
   \@esphack}
\makeatother
\endinput
%%
%% End of file `newslide.cls'.
