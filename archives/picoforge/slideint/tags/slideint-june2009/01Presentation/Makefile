.SUFFIXES: # Delete the default suffixes

SHELL   = /bin/bash
LATEXTEX4HT = ./ConfigTex4ht/latextex4ht.sh
MAKEINDEXTEX4HT = ./ConfigTex4ht/makeindextex4ht.sh
TEX4HT = ./ConfigTex4ht/tex4ht.sh

BASEDIAPOS=diapos
BASECHAP= chap
BASECHAPENS= chapens
BASEDEUXPP= diapos_2pp
BASEHTML= html
SOURCES = $(filter-out $(wildcard toc_*.tex) tochtml.tex tocstoc.tex \
	chapens.tex chap.tex diapos.tex \
	diapos_2pp.tex html.tex, \
	$(wildcard *.tex))
ENLIGNE = Web
LASTHTML= $(SOURCES) lastHTML.date
NEEDHTML= $(word 1,$(shell ls -1 --sort=t $(LASTHTML) 2> /dev/null))
BIBS	= $(wildcard *.bib)
FIGDIR 	= Figures
FIGURES	= $(wildcard $(FIGDIR)/*.fig)
ifneq ($(VERBOSE),)
LATEXREDIRECT = 
else
LATEXREDIRECT = > /dev/null
VOIRCOMM = @
endif

include Makefile-configure

voirvar :
	@echo Makefile : BASEDIAPOS = $(BASEDIAPOS)
	@echo Makefile : BASECHAP = $(BASECHAP)
	@echo Makefile : BASECHAPENS = $(BASECHAPENS)
	@echo Makefile : BASEHTML = $(BASEHTML)
	@echo Makefile : SOURCES = $(SOURCES)
	@echo Makefile : LASTHTML = $(LASTHTML)
	@echo Makefile : BIBS = $(BIBS)
	@echo Makefile : FIGURES = $(FIGURES)
	@echo Makefile : NEEDHTML = $(NEEDHTML)
	@echo Makefile : LATEXREDIRECT = $(LATEXREDIRECT)
	@echo Makefile : VOIRCOMM = $(VOIRCOMM)
	make post-voirvar

all:	pre-all diapos chap chapens html post-all

tout:	all

diapos:	lightclean pre-diapos $(BASEDIAPOS).ps $(BASEDIAPOS).pdf post-diapos

chap:	lightclean pre-chap $(BASECHAP).ps $(BASECHAP).pdf post-chap

chapens: lightclean pre-chapens $(BASECHAPENS).ps $(BASECHAPENS).pdf post-chapens

cleanall: pre-cleanall clean
	$(VOIRCOMM)rm -f *.ps *.pdf *.tgz *.dvi lastHTML.date
	$(VOIRCOMM)rm -rf $(ENLIGNE)
	$(VOIRCOMM)make -C ConfigTex4ht cleanall

clean:	pre-clean lightclean
	$(VOIRCOMM)rm -f $(FIGDIR)/[!_]*.eps $(FIGDIR)/[!_]*.png \
	$(FIGDIR)/*.bak

lightclean: pre-lightclean
	$(VOIRCOMM)rm -f *.log *.aux *.bbl *.blg *.toc *~ *.bak *.out \
		*.idx *.ind *.ilg *.tmp *.lof *.lot tochtml.tex toc_*.tex \
		*_tmp.ps *.css *.idv *.lg *.xref *ppm *4ct *4tc *.fls \
		*-js.js *-js.tex *.html *.4dx *.4ix *.5dx *.delme \
		$(FIGDIR)/*.fig.bak
	$(VOIRCOMM)rm -rf Tidy InsertionTitre CarAccentISO

%.dvi:	$(SOURCES) $(BIBS) $(FIGURES:%.fig=%.eps)
ifneq ($(DOPS)$(DOPDF),)
	$(VOIRCOMM)latex -file-line-error $*.tex $(LATEXREDIRECT)
	$(VOIRCOMM)if grep -q '\\bibliography{' $(SOURCES); \
		then bibtex $* $(LATEXREDIRECT); true \
		else true; \
	fi
	$(VOIRCOMM)if grep -q '\\slideprintindex' $(SOURCES); \
		then makeindex -L -s index.isty -t index.ilg $* \
			$(LATEXREDIRECT); true \
		else true; \
	fi
	$(VOIRCOMM)-make other_bibtex BIBTEXFILE=$*
	$(VOIRCOMM)-make other_makeindex MAKEINDEXFILE=$*
	$(VOIRCOMM)[[ -a $*.bbl || -a $*.ilg ]] && \
		(latex -file-line-error $*.tex $(LATEXREDIRECT)); true
	$(VOIRCOMM)latex -file-line-error $*.tex $(LATEXREDIRECT)
	$(VOIRCOMM)for i in toc_*.tex ; \
	do\
		cp $$i tmp.tex ; \
		uniq tmp.tex > $$i ; \
		rm -f tmp.tex ; \
	done
	$(VOIRCOMM)latex -file-line-error $*.tex $(LATEXREDIRECT)
else
	$(VOIRCOMM)true
endif
	$(VOIRCOMM)make post-dvi

%.ps:	%.dvi
ifneq ($(DOPS),)
	$(VOIRCOMM)dvips -o $*.ps $*.dvi
else
	$(VOIRCOMM)true
endif

%.pdf:	%.dvi
ifneq ($(DOPDF),)
ifeq ($(PS2PDF_DVIPDFM),$(DVIPDFM))
	$(VOIRCOMM)dvipdfm -p a4 -z9 -o $*.pdf $*.dvi
else
	$(VOIRCOMM)dvips -o $*.ps $*.dvi
	$(VOIRCOMM)ps2pdf -sPAPERSIZE=a4 -dPDFsettings=/prepress $*.ps $*.pdf
endif
else
	$(VOIRCOMM)true
endif

deuxpp:	pre-deuxpp lightclean $(BASEDEUXPP).dvi
ifneq ($(DOPS)$(DOPDF),)
	$(VOIRCOMM)[[ ! -e $(BASEDEUXPP).ps || \
		$(BASEDEUXPP).ps -ot $(BASEDEUXPP).dvi ]] && \
		(dvips -o $(BASEDEUXPP)_tmp.ps $(BASEDEUXPP).dvi; \
		 psnup -2 -r -m1cm -d $(BASEDEUXPP)_tmp.ps $(BASEDEUXPP).ps; \
		 rm $(BASEDEUXPP)_tmp.ps); true
ifneq ($(DOPDF),)
	$(VOIRCOMM)[[ ! -e diapos_2pp.pdf || \
		diapos_2pp.pdf -ot diapos_2pp.dvi ]] && \
		(ps2pdf -sPAPERSIZE=a4 -dPDFsettings=/prepress \
			diapos_2pp.ps diapos_2pp.pdf); true
ifeq ($(DOPS),)
	$(VOIRCOMM)rm -f diapos_2pp.ps
else
	$(VOIRCOMM)true
endif
else
	$(VOIRCOMM)true
endif
else
	$(VOIRCOMM)true
endif
	$(VOIRCOMM)make post-deuxpp

%.eps: %.fig
	$(VOIRCOMM)fig2dev -L eps $< $@

%.png: %.fig
	$(VOIRCOMM)fig2dev -L png $< $@

tgz:	
	$(VOIRCOMM)tar zcvf all.tgz * --exclude .svn --exclude *.tgz

html:	lightclean pre-html $(FIGURES:%.fig=%.eps) $(FIGURES:%.fig=%.png)
ifneq ($(DOHTML),)
ifneq ($(NEEDHTML),lastHTML.date)
	$(VOIRCOMM)rm -rf $(ENLIGNE)
	$(VOIRCOMM)install -d $(ENLIGNE)/ \
		$(ENLIGNE)/Images \
		$(ENLIGNE)/Figures Tidy InsertionTitre CarAccentISO
	$(VOIRCOMM)ls Images/*.gif > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && \
		 cp Images/*.gif $(ENLIGNE)/Images;\
		 true;
	$(VOIRCOMM)ls Images/*.png > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && \
		 cp Images/*.png $(ENLIGNE)/Images;\
		 true;
	$(VOIRCOMM)if grep -q '\\bibliography{' $(SOURCES); \
		then touch bib.delme; \
		else true; \
	fi
	$(VOIRCOMM)if grep -q '\\slideprintindex' $(SOURCES); \
		then touch index.delme; \
		else true; \
	fi
	$(VOIRCOMM)${LATEXTEX4HT} ${BASEHTML} "ConfigTex4ht/tex4ht.cfg" \
		$(LATEXREDIRECT)
	$(VOIRCOMM)[[ -a bib.delme && ! -a index.delme ]] && \
		(${LATEXTEX4HT} ${BASEHTML} \
			"ConfigTex4ht/tex4ht.cfg" $(LATEXREDIRECT)); true
	$(VOIRCOMM)[[ -a index.delme ]] && \
		(${LATEXTEX4HT} ${BASEHTML} \
			"ConfigTex4ht/tex4ht.cfg" $(LATEXREDIRECT); \
		 ${LATEXTEX4HT} ${BASEHTML} \
			"ConfigTex4ht/tex4ht.cfg" $(LATEXREDIRECT)); true
	$(VOIRCOMM)[[ -a bib.delme ]] && \
		(bibtex  ${BASEHTML} $(LATEXREDIRECT)); true
	$(VOIRCOMM)[[ -a index.delme ]] && \
		(${MAKEINDEXTEX4HT} ${BASEHTML} $(LATEXREDIRECT)); true
	$(VOIRCOMM)${LATEXTEX4HT} ${BASEHTML} \
		"ConfigTex4ht/tex4ht.cfg" $(LATEXREDIRECT)
	$(VOIRCOMM)${LATEXTEX4HT} ${BASEHTML} \
			"ConfigTex4ht/tex4ht.cfg" $(LATEXREDIRECT)
	$(VOIRCOMM)[[ -a index.delme ]] && \
		(${LATEXTEX4HT} ${BASEHTML} \
			"ConfigTex4ht/tex4ht.cfg" $(LATEXREDIRECT)); true
	$(VOIRCOMM)${TEX4HT} ${BASEHTML} $(LATEXREDIRECT)
	$(VOIRCOMM)-mv *.html Tidy
	$(VOIRCOMM)-mv *.css Tidy
	$(VOIRCOMM)ls *.png > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && mv -f *.png Tidy; true;
	$(VOIRCOMM)ls *.gif > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && mv -f *.gif Tidy; true;
	$(VOIRCOMM)cp ConfigTex4ht/tidy.cfg Tidy
	$(VOIRCOMM)(cd Tidy ; \
	for i in *.html ;\
	do\
		tidy -config tidy.cfg -f tidy.log -o \
			../InsertionTitre/$$i $$i ; \
		if (( $$? == 1 )) ; \
			then \
				echo \\n\\n\*\*\* file $$i >> \
					tidy_all.log ; \
				cat tidy.log >> tidy_all.log ; \
		fi ; \
		if (( $$? == 2 )) ; \
			then \
				echo tidy: \*\*\* Error: file $$i ; \
		fi ; \
	done ; \
	cd ..)
	$(VOIRCOMM)rm Tidy/tidy.cfg
	$(VOIRCOMM)cp Tidy/*.css InsertionTitre
	$(VOIRCOMM)ls Tidy/*.png > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp -f Tidy/*.png InsertionTitre; true;
	$(VOIRCOMM)ls Tidy/*.gif > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp -f Tidy/*.gif InsertionTitre; true;
	$(VOIRCOMM)cp Tidy/*.css $(ENLIGNE)
	$(VOIRCOMM)ls Tidy/*.png > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp Tidy/*.png $(ENLIGNE); true;
	$(VOIRCOMM)ls Tidy/*.gif > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp Tidy/*.gif $(ENLIGNE); true;
	$(VOIRCOMM)ls Tidy/${BASEHTML}*.html > /dev/null 2> /dev/null; \
		EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp Tidy/${BASEHTML}*.html \
			$(ENLIGNE); true;
	$(VOIRCOMM)-cp -f Tidy/${BASEHTML}*.html $(ENLIGNE)
	$(VOIRCOMM)-recode -d ISO-8859-15..HTML_4.0 \
		$(ENLIGNE)/${BASEHTML}*.html
	$(VOIRCOMM)ln -f -s ${BASEHTML}.html InsertionTitre/index.html
	$(VOIRCOMM)make -C ConfigTex4ht TITRE=${TITRE} \
		LONGUEUR_MENU=${LONGUEUR_MENU} \
		ORIGINE=InsertionTitre INTERMEDIAIRE=CarAccentISO \
		FINAL=$(ENLIGNE)
	$(VOIRCOMM)recode -d ISO-8859-15..HTML_4.0 \
		InsertionTitre/${BASEHTML}.css
	$(VOIRCOMM)cp InsertionTitre/${BASEHTML}.css $(ENLIGNE)
	$(VOIRCOMM)-rm $(ENLIGNE)/${BASEHTML}.html
	$(VOIRCOMM)cp ConfigTex4ht/*.css $(ENLIGNE)
	$(VOIRCOMM)cp ConfigTex4ht/*.html $(ENLIGNE)
	$(VOIRCOMM)ls Figures/*.png > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp Figures/*.png \
			$(ENLIGNE)/Figures; true;
	$(VOIRCOMM)touch lastHTML.date
	$(VOIRCOMM)make post-html
else
	$(VOIRCOMM)true
endif
else
	$(VOIRCOMM)true
endif
