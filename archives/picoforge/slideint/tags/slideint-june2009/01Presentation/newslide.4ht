% -*-latex-*-
% Contient les compl�ments de traduction de newslide.cls
% nom du fichier courant
\def\theslidefilename{}
\def\slidefilename#1{\gdef\theslidefilename{#1}}
% nom du titre de la page courante sans les num�ros
\def\thetitlewonum{}
\def\titlewonum#1{\gdef\thetitlewonum{#1}}
% fichier table des mati�res pour le menu
\newwrite\tochtml
\openout\tochtml=tochtml
% pour savoir si c'est une table des matieres
\newcounter{intoc}
\renewcommand\intocslide{\setcounter{intoc}{1}}
\setcounter{intoc}{0}
% Pour mettre les liens suivant et pr�cedent en bas de page dans TeX4ht
\newcommand{\prevnextlinks}{%
  \HCode{<p>}%
  ~~~%
  \HCode{<a href="}%
  \ifthenelse{\equal{\prevCutAt}{\empty}}%
	     {index.html}%
	     {\prevCutAt}%
  \HCode{")><span class="crosslink">}~pr�c�dent~\HCode{</span></a>}%
  \ifthenelse{\equal{\nextCutAt}{\empty}}%
	     {}%
	     {~~~%
	       \HCode{<a href="}%
	       \nextCutAt%
	       \HCode{"><span class="crosslink">}~suivant~\HCode{</span></a>}
	     }%
}
% simple formating of section, subsection and subsubsection
\newcommand{\simplesection}[1]{\HCode{<h3 class="sectionHead">}%
   \addtocounter{tocsection}{1}%
   \thetocsection\HCode{&nbsp;}#1%
   \HCode{</h3>}}
\newcommand{\simplesubsection}[1]{\HCode{<h3 class="subsectionHead">}%
   \addtocounter{tocsubsection}{1}%
   \thetocsection.\thetocsubsection\HCode{&nbsp;}#1%
   \HCode{</h3>}}
\newcommand{\simplesubsubsection}[1]{\HCode{<h3 class="subsubsectionHead">}%
   \addtocounter{tocsubsubsection}{1}%
   \thetocsection.\thetocsubsection.\thetocsubsubsection\HCode{&nbsp;}#1%
   \HCode{</h3>}}
\renewcommand{\renewsectionoriginformat}{%
   \let\section\simplesection%
   \let\subsection\simplesubsection%
   \let\subsubsection\simplesubsubsection%
   \renewcommand{\tableofcontents}{}}
% inhibition des overlays
\renewenvironment{overlay}[1]{}{}
% Attention ! Les compteurs 'tocsection', 'tocsubsection'et
% 'tocsubsubsection' continuent d'�voluer (d'�tre incr�ment�s)
% pour les environnements *. Ils ne correspondent donc pas aux num�ros
% de section standards (ils �voluent plus vite). Et ne jamais les
% remettre � 0.
\newwrite\tocsectionhtml
\newwrite\tocsubsectionhtml
% !!! je ne sais pas pourquoi comportement diff�rent entre cls et 4ht
% dans le fait d'avoir input avant ou apr�s openout
% donc, dans le begingroup ou dans le endgroup
\renewenvironment{secslide}[2][slidetitletoc]{%
  \titlewonum{#2}%
  \titlesecslide{}\titlesubsecslide{}\titlesubsubsecslide{}%
  \closeout\tocsectionhtml%
  \addtocounter{tocsection}{1}%
  \slidenumero{\thetocsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \centering\protect\section{#2}%
  \titlesecslide{\thesection ~ #2}%
  \write\tocall{~ \thesection ~ \noexpand\HCode\noexpand{<a href=\noexpand"\theslidefilename">}#2\noexpand\HCode\noexpand{</a>\noexpand}}%
  \write\tocall{\noexpand\\}%
  \ifthenelse{\equal{\theintoc}{1}}%
	     {\write\tochtml{SECTION}\setcounter{intoc}{0}}%
	     {\write\tochtml{section}}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{100}%
  \write\tochtml{100}%
  \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tochtml{\thesection \noexpand&nbsp; #2}}{\write\tochtml{\thesection \noexpand&nbsp; #1}}%
  \write\tochtml{\theslidefilename}}%
		 {\openout\tocsectionhtml=toc_sec_\thetocsection%
		  \prevnextlinks\newpage}
\renewenvironment{Psecslide}[2][slidetitletoc]{%
  \titlewonum{#2}%
  \titlesecslide{}\titlesubsecslide{}\titlesubsubsecslide{}%
  \closeout\tocsectionhtml%
  \addtocounter{tocsection}{1}%
  \slidenumero{\thetocsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \centering\protect\section{#2}%
  \titlesecslide{\thesection ~ #2}%
  \write\tocall{~ \thesection ~ \noexpand\HCode\noexpand{<a href=\noexpand"\theslidefilename">}#2\noexpand\HCode\noexpand{</a>\noexpand}}%
  \write\tocall{\noexpand\\}%
  \ifthenelse{\equal{\theintoc}{1}}%
	     {\write\tochtml{SECTION}\setcounter{intoc}{0}}%
	     {\write\tochtml{section}}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{100}%
  \write\tochtml{100}%
  \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tochtml{\thesection \noexpand&nbsp; #2}}{\write\tochtml{\thesection \noexpand&nbsp; #1}}%
  \write\tochtml{\theslidefilename}}%
		 {\openout\tocsectionhtml=toc_sec_\thetocsection%
		  \prevnextlinks\newpage}
\renewenvironment{secslide*}[2][slidetitletoc]{%
  \titlewonum{#2}%
  \titlesecslide{}\titlesubsecslide{}\titlesubsubsecslide{}%
  \closeout\tocsectionhtml%
  \addtocounter{tocsection}{1}%
  \slidenumero{\thetocsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \centering\protect\section*{#2}%
  \titlesecslide{#2}%
  \ifthenelse{\equal{\theintoc}{1}}%
	     {\write\tochtml{SECTION}\setcounter{intoc}{0}}%
	     {\write\tochtml{section}}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{100}%
  \write\tochtml{100}%
  \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tochtml{#2}}{\write\tochtml{#1}}%
  \write\tochtml{\theslidefilename}}%
		 {\openout\tocsectionhtml=toc_sec_\thetocsection%
		  \prevnextlinks\newpage}
\renewenvironment{Psecslide*}[2][slidetitletoc]{%
  \titlewonum{#2}%
  \titlesecslide{}\titlesubsecslide{}\titlesubsubsecslide{}%
  \closeout\tocsectionhtml%
  \addtocounter{tocsection}{1}%
  \slidenumero{\thetocsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \centering\protect\section*{#2}%
  \titlesecslide{#2}%
  \ifthenelse{\equal{\theintoc}{1}}%
	     {\write\tochtml{SECTION}\setcounter{intoc}{0}}%
	     {\write\tochtml{section}}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{100}%
  \write\tochtml{100}%
  \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tochtml{#2}}{\write\tochtml{#1}}%
  \write\tochtml{\theslidefilename}}%
		 {\openout\tocsectionhtml=toc_sec_\thetocsection%
		  \prevnextlinks\newpage}
\renewenvironment{subsecslide}[2][slidetitletoc]{%
  \titlewonum{#2}%
  \titlesubsecslide{}\titlesubsubsecslide{}%
  \closeout\tocsubsectionhtml%
  \addtocounter{tocsubsection}{1}%
  \slidenumero{\thetocsection.\thetocsubsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \centering\protect\subsection{#2}%
  \titlesubsecslide{\thesubsection ~ #2}%
  \write\tocsectionhtml{~ \thesubsection ~ \noexpand\HCode\noexpand{<a href=\noexpand"\theslidefilename">}#2\noexpand\HCode\noexpand{</a>\noexpand}}%
  \write\tocsectionhtml{\noexpand\\}%
  \ifthenelse{\equal{\theintoc}{1}}%
	     {\write\tochtml{SUBSECTION}\setcounter{intoc}{0}}%
	     {\write\tochtml{subsection}}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{\thetocsubsection}%
  \write\tochtml{100}%
  \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tochtml{\thesubsection \noexpand&nbsp; #2}}{\write\tochtml{\thesubsection \noexpand&nbsp; #1}}%
  \write\tochtml{\theslidefilename}}%
		 {\openout\tocsubsectionhtml=toc_sub_sec_\thetocsubsection%
		  \prevnextlinks\newpage}
\renewenvironment{Psubsecslide}[2][slidetitletoc]{%
  \titlewonum{#2}%
  \titlesubsecslide{}\titlesubsubsecslide{}%
  \closeout\tocsubsectionhtml%
  \addtocounter{tocsubsection}{1}%
  \slidenumero{\thetocsection.\thetocsubsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \centering\protect\subsection{#2}%
  \titlesubsecslide{\thesubsection ~ #2}%
  \write\tocsectionhtml{~ \thesubsection ~ \noexpand\HCode\noexpand{<a href=\noexpand"\theslidefilename">}#2\noexpand\HCode\noexpand{</a>\noexpand}}%
  \write\tocsectionhtml{\noexpand\\}%
  \ifthenelse{\equal{\theintoc}{1}}%
	     {\write\tochtml{SUBSECTION}\setcounter{intoc}{0}}%
	     {\write\tochtml{subsection}}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{\thetocsubsection}%
  \write\tochtml{100}%
  \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tochtml{\thesubsection \noexpand&nbsp; #2}}{\write\tochtml{\thesubsection \noexpand&nbsp; #1}}%
  \write\tochtml{\theslidefilename}}%
		 {\openout\tocsubsectionhtml=toc_sub_sec_\thetocsubsection%
		  \prevnextlinks\newpage}
\renewenvironment{subsecslide*}[2][slidetitletoc]{%
  \titlewonum{#2}%
  \titlesubsecslide{}\titlesubsubsecslide{}%
  \closeout\tocsubsectionhtml%
  \addtocounter{tocsubsection}{1}%
  \slidenumero{\thetocsection.\thetocsubsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \centering\protect\subsection*{#2}%
  \titlesubsecslide{#2}%
  \ifthenelse{\equal{\theintoc}{1}}%
	     {\write\tochtml{SUBSECTION}\setcounter{intoc}{0}}%
	     {\write\tochtml{subsection}}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{\thetocsubsection}%
  \write\tochtml{100}%
  \write\tochtml{#1}%
  \write\tochtml{\theslidefilename}}%
		 {\openout\tocsubsectionhtml=toc_sub_sec_\thetocsubsection%
		  \prevnextlinks\newpage}
\renewenvironment{Psubsecslide*}[2][slidetitletoc]{%
  \titlewonum{#2}%
  \titlesubsecslide{}\titlesubsubsecslide{}%
  \closeout\tocsubsectionhtml%
  \addtocounter{tocsubsection}{1}%
  \slidenumero{\thetocsection.\thetocsubsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \centering\protect\subsection*{#2}%
  \titlesubsecslide{#2}%
  \ifthenelse{\equal{\theintoc}{1}}%
	     {\write\tochtml{SUBSECTION}\setcounter{intoc}{0}}%
	     {\write\tochtml{subsection}}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{\thetocsubsection}%
  \write\tochtml{100}%
  \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tochtml{#2}}{\write\tochtml{#1}}%
  \write\tochtml{\theslidefilename}}%
		 {\openout\tocsubsectionhtml=toc_sub_sec_\thetocsubsection%
		  \prevnextlinks\newpage}
\renewenvironment{subsubsecslide}[2][slidetitletoc]{%
  \titlewonum{#2}%
  \titlesubsubsecslide{}%
  \addtocounter{tocsubsubsection}{1}%
  \slidenumero{\thetocsection.\thetocsubsection.\thetocsubsubsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \centering\protect\subsubsection{#2}%
  \titlesubsubsecslide{\thesubsubsection ~ #2}%
  \write\tocsubsectionhtml{~ \thesubsubsection ~ \noexpand\HCode\noexpand{<a href=\noexpand"\theslidefilename">}#2\noexpand\HCode\noexpand{</a>\noexpand}}%
  \write\tocsubsectionhtml{\noexpand\\}%
  \write\tochtml{subsubsection}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{\thetocsubsection}%
  \write\tochtml{\thetocsubsubsection}%
  \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tochtml{\thesubsubsection \noexpand&nbsp; #2}}{\write\tochtml{\thesubsubsection \noexpand&nbsp; #1}}%
  \write\tochtml{\theslidefilename}}%
		 {\prevnextlinks\newpage}
\renewenvironment{Psubsubsecslide}[2][slidetitletoc]{%
  \titlewonum{#2}%
  \titlesubsubsecslide{}%
  \addtocounter{tocsubsubsection}{1}%
  \slidenumero{\thetocsection.\thetocsubsection.\thetocsubsubsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \centering\protect\subsubsection{#2}%
  \titlesubsubsecslide{\thesubsubsection ~ #2}%
  \write\tocsubsectionhtml{~ \thesubsubsection ~ \noexpand\HCode\noexpand{<a href=\noexpand"\theslidefilename">}#2\noexpand\HCode\noexpand{</a>\noexpand}}%
  \write\tocsubsectionhtml{\noexpand\\}%
  \write\tochtml{subsubsection}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{\thetocsubsection}%
  \write\tochtml{\thetocsubsubsection}%
  \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tochtml{\thesubsubsection \noexpand&nbsp; #2}}{\write\tochtml{\thesubsubsection \noexpand&nbsp; #1}}%
  \write\tochtml{\theslidefilename}}%
		 {\prevnextlinks\newpage}
\renewenvironment{subsubsecslide*}[2][slidetitletoc]{%
  \titlewonum{#2}%
  \titlesubsubsecslide{}%
  \addtocounter{tocsubsubsection}{1}%
  \slidenumero{\thetocsection.\thetocsubsection.\thetocsubsubsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \centering\protect\subsubsection*{#2}%
  \titlesubsubsecslide{#2}%
  \write\tochtml{subsubsection}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{\thetocsubsection}%
  \write\tochtml{\thetocsubsubsection}%
  \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tochtml{#2}}{\write\tochtml{#1}}%
  \write\tochtml{\theslidefilename}}%
		 {\prevnextlinks\newpage}
\renewenvironment{Psubsubsecslide*}[2][slidetitletoc]{%
  \titlewonum{#2}%
  \titlesubsubsecslide{}%
  \addtocounter{tocsubsubsection}{1}%
  \slidenumero{\thetocsection.\thetocsubsection.\thetocsubsubsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \centering\protect\subsubsection*{#2}%
  \titlesubsubsecslide{#2}%
  \write\tochtml{subsubsection}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{\thetocsubsection}%
  \write\tochtml{\thetocsubsubsection}%
  \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tochtml{#2}}{\write\tochtml{#1}}%
  \write\tochtml{\theslidefilename}}%
		 {\prevnextlinks\newpage}

\endinput
