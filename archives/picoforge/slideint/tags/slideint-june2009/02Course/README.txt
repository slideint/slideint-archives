Structure de l'arborescence 04CoursComplet
==========================================

L'arborescence comporte une partie d�di�e au contenu et une partie
plus g�n�rique, d�di�e � la mise en forme pour la production de
polycopi�s ou de sites en ligne. La s�paration entre ces deux parties
est forte dans la mesure o� tous les fichiers d�crivant des
traitements et tous les Makefile associ�s sont dans la partie "mise en
forme". Des liens temporaires sont cr��s � la demande (par les
diff�rents Makefile) dans les r�pertoires de la partie "contenu" pour
donner acc�s aux Makefile n�cessaires lors de la production de
polycopi�s ou de cours en ligne. En cons�quence, les fichiers de style
et de classe LaTeX ainsi que les autres fichiers et r�pertoires utiles
pour les g�n�rations PostScript, PDF ou HTML, ne sont pas dupliqu�s
dans les r�pertoires de contenu, mais tous rassembl�s dans le
r�pertoire Ressources. Cela facilite grandement les changements de
version depuis le projet slideint.

1. Partie "contenu" de l'arborescence
-------------------------------------

Le contenu est propre au cours , mais sa structuration est
g�n�rique : les items sont regroup�s en collections homog�nes tant du
point de vue de leur utilisation p�dagogique (cours, �nonc�s de TP,
corrig�s de TP, etc.) que des traitements � leur appliquer pour leur
mise en forme, d�crits dans les Makefile. Les noms des r�pertoires
correspondant aux colletions et items sont configurables en �ditant le
fichier Makefile-configure mentionn� plus loin.

L'arborescence comporte au plus haut niveau (appel� r�pertoire racine
dans la suite) des r�pertoires correspondant aux cat�gories suivantes :
- Cours, avec un sous-r�pertoire par cours (C1, C2, C3, C4, C5-6, etc.),
- EnoncesTD, avec un sous-r�pertoire par TD (ETD1-2, ETD3-4, etc.),
- CorrigesTD, idem (CTD1-2, CTD3-4, etc.),
- EnoncesTP, idem (ETP1-2, ETP3-4, etc.),
- CorrigesTP, idem (CTP1-2, CTP3-4, etc.),
- Controles, avec un sous-r�pertoire par ann�e scolaire (2005-2006, etc.),
- Divers : autres documents destin�s � �tre incorpor�s aux polycopi�s
  ou au site de publication (r�pertoire Public) apr�s traitement (FAQ,
  etc.),
- Poly : fichiers pour la construction � la vol�e du source tex du
  polycopi�, et Makefile associ�.
- Documents : documents de gestion p�dagogique int�gr�s sans
  traitement particulier au site de publication.

� l'exclusion du r�pertoire Documents, chacun de ces r�pertoires
comporte une collection de sous-r�pertoires structur�s � l'identique :
- un fichier contenu.tex correspondant au texte de l'item (par exemple
  C3/contenu.tex). Bien s�r, rien n'emp�che de modulariser l'�criture
  du chapitre en r�partissant le code LaTeX dans diff�rents fichiers
  inclus avec la commande \input dans le fichier contenu.tex ;
- un r�pertoire facultatif Figures, avec les .fig utilis�s par le
  fichier contenu.tex pr�c�dent ;
- d'autres fichiers et r�pertoires ajout�s � la convenance : par
  exemple, pour une bibliographie propre au chapitre, l'ajout de
  fichiers de code C.


2. Partie "mise en forme" de l'arborescence
-------------------------------------------

Deux r�pertoires d�di�s � la mise en forme sont pr�sents � la racine
de l'arborescence :
- Ressources, avec des fichiers de donn�es ou de traitement, certains
  g�n�riques (newslide.cls, newslide.4ht, etc.) et d'autres propres �
  un cours (par exemple ap11.sty et ap11.4ht dans le cours AP11),
- Public, coquille vide remplie � la demande par activation de
  fichiers Makefile.

Chaque sous-r�pertoire du r�pertoire Ressources joue un r�le bien
d�fini dans la cha�ne de production selon son contenu :
- Common-tex : wrappers g�n�riques pour les fichiers contenu.tex,
  selon le type de r�sultat souhait� :
  - common.tex : transparents,
  - common-notes.tex : polycopi� de l'�tudiant,
  - common-notes-ens.tex : polycopi� de l'enseignant,
  - common-html.tex : pages HTML.
- Images : images g�n�riques pour la mise en ligne avec suivi, et
  Makefile associ�,
  - avec les fichiers sp�cifiques suivants : les bandeaux aux formats
    eps, jpg et png.
- ConfigTex4ht : donn�es et traitements g�n�riques pour la production
  de pages html de cours en ligne avec Tex4ht,
Par ailleurs, le r�pertoire Ressources comporte :
- le fichier Makefile-figures g�n�rique pour les r�pertoires Figures
  facultatifs mentionn�s pr�c�demment,
- le fichier Makefile g�n�rique pour les diff�rentes cibles des
  r�pertoires contenu du cours
- et le Makefile-configure contenant les �l�ments sp�cifiques pour ce
  cours, en particulier la description de l'arborescence de contenu.
Ces fichiers et r�pertoires sont syst�matiquement utilis�s gr�ce � des
liens cr��s dans les r�pertoires de contenu concern�s au moment de leur
exploitation pour la production de polycopi� ou de pages HTML, et
supprim�s lors d'un nettoyage.

Last but not least, le r�pertoire racine contient le Makefile
principal (dit Makefile racine dans la suite, qui permet de produire
les chapitres d'un polycopi� ou d'un cours en ligne selon la cible
choisie).

3. Organisation de la cha�ne de production
------------------------------------------

Toute production est op�r�e � partir du r�pertoire racine. Afin de
donner la possibilit� de produire tout ou partie des chapitres et de
d�signer un item par son nom sans imposer � l'utilisateur de saisir
son chemin d'acc�s avec sa cat�gorie :
- les items ont tous un identificateur unique (C3, ETD1-2, CTD1-2, etc.),
- leur liste par cat�gorie (cours, �nonc�s de TP, etc.) est consign�e
  dans le fichier Makefile-configure du r�pertoire racine,
- le Makefile principal exploite ce fichier Makefile-configure pour
  d�terminer, � partir du nom de l'item, les informations utiles � la
  mise en place des liens n�cessaires.

Le Makefile racine propose des cibles qui peuvent s'utiliser avec ou
sans macro LIST : slides, chap, chapens, html, tout, public, clean,
cleanall, cleanpublic. Sans macro, l'effet de la cible est appliqu�e �
l'ensemble des chapitres, y compris le polycopi�.

Lorsqu'il utilise les cibles avec la macro LIST, l'utilisateur indique
comme valeur de la macro une liste d'items s�par�s par des
virgules pour pr�ciser les items concern�s. Ainsi, la commande
make chap LIST=C1,C3
pr�pare le chapitre, version �tudiant, des items C1 et C3. Pour
chaque item, le Makefile principal cr�e les liens n�cessaires dans
les r�pertoires correspondant (par exemple, Cours/C1 et
Cours/C1/Figures). L'un de ces liens pointe vers le fichier Makefile
g�n�rique du r�pertoire Ressources. Ensuite, le script active la cible
correspondante (ici, chap) du Makefile g�n�rique � partir des
r�pertoires des items (par exemple, cd Cours/C1 ; make chap). Enfin,
le polycopi� est un item comme les autres : par exemple la commande
make chap LIST=Poly 
pr�pare le polycopi�, version �tudiante (chapens pour la version
enseignante). En ce qui concerne la liste des items inclus dans les
polycopi�s, voir les d�finitions POLY et POLYENS du fichier
Makefile-configure du r�pertoire racine. Bien s�r, le traitement du
polycopi� est un peu particulier, se r�f�rer au code source perl pour
les d�tails.

Lancer make dans le r�pertoire racine affiche le r�le des diff�rentes
cibles.

Par d�faut, les affichages en sortie standard des commandes LaTeX et
TeX4ht sont redirig�es vers /dev/null. Vous pouvez dans ce cas vous
reporter aux d�tails des .log. Pour supprimer la redirection, affectez
la macro VERBOSE � une valeur non nulle, comme ceci par exemple :
$ make chap LIST=C1 VERBOSE=yes

4. Configuration de la cha�ne de production
-------------------------------------------

