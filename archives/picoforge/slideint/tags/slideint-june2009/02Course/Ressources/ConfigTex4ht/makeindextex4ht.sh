# see comments in .log concerning \beforeentry
# as defaulted in index.sty and makeindex, the symbol '|' indicates that
# the rest of the argument list is to be used as the encapsulating command
# for the page  number.
# since the first command 'tex...' adds a command LNK defined in idxmake
# that encapsulates the page number, index entries with already another
# command such as '(', ')', and 'see' are REJECTED
      tex '\def\filename{{'$1'}{idx}{4dx}{ind}} \input idxmake.4ht'
      cp $1.4dx $1.5dx
      sed 's/\^\^e9/�/g' $1.5dx | sed 's/\^\^e8/�/g' | sed 's/\^\^ea/�/g' | sed 's/\^\^e0/�/g' | sed 's/\^\^e2/�/g' | sed 's/\^\^e7/�/g' | sed 's/\^\^f4/�/g' | sed 's/\^\^f9/�/g' | sed 's/\^\^ef/�/g' | sed 's/\^\^ee/�/g' | sed 's/\^\^c9/�/g' | sed 's/\^\^c8/�/g' > $1.4dx
      makeindex -t $1.ilg -o $1.ind $1.4dx

#tex '\def\filename{{'$1'}{idx}{4dx}{ind}} \input idxmake.4ht'
##makeindex -s index.4ht.isty -t $1.ilg -o $1.ind $1.4dx
#recode -d ISO-8859-15..LaTeX $1.4dx
#makeindex -L -o $1.ind $1.4dx
