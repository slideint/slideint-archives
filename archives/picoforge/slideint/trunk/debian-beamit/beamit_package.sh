#!/bin/bash

if [ $# -ne 2 ]
then
    echo "Usage $0 version repository"
    echo "where  version is in the form yearmonthday"
    echo "and repository is the directory in which bin packages will be stored"
    exit 1
fi
# $1 is the name of the version, in the form yearmonthday
# $2 is the location of the apt repository where to deploy the package
echo '$1' = $1
echo '$2' = $2
# even if the directory debian is not part of the package, export it
rm -rf /tmp/beamit-*
cd ../..
svn export trunk /tmp/beamit-debian-$1
svn export web/man-beamit /tmp/beamit-debian-$1/Man
svn export web/tutorials-beamit /tmp/beamit-debian-$1/tutorials
cd /tmp/beamit-debian-$1
mv debian-beamit debian
mkdir examples-beamit/fulfledged
mv examples-beamit/presentation examples-beamit/fulfledged/.
mv tutorials examples-beamit/.
cd /tmp/beamit-debian-$1/debian
# fill in the new version change log
dch -v $1
cp changelog /tmp
cd ..
# -us: non signed source package; -uc: non signed file .changes
dpkg-buildpackage -rfakeroot
rm -f $2/binary/beamit_*_all.deb
cp ../beamit_$1_all.deb $2/binary/
rm -f $2/sources/beamit_*
cp ../beamit_$1.tar.gz ../beamit_$1.dsc $2/sources/
cd $2
dpkg-scanpackages binary /dev/null | gzip -9c > binary/Packages.gz
chmod a+r binary/*
dpkg-scansources sources /dev/null | gzip -9c > sources/Sources.gz
chmod a+r sources/*
#rm -rf /tmp/beamit*
