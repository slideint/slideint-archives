.SUFFIXES: # Delete the default suffixes

SHELL   = /bin/bash
LATEXTEX4HT = ./ConfigTex4ht/latextex4ht.sh
MAKEINDEXTEX4HT = ./ConfigTex4ht/makeindextex4ht.sh
TEX4HT = ./ConfigTex4ht/tex4ht.sh

BASEDIAPOS=diapos
BASECHAP= chap
BASECHAPENS= chapens
BASEDIAPOSNB= diaposnb
BASEHTML= html
ENLIGNE = Web

OTHER_DEPENDENCIES +=
BIBS	= $(wildcard *.bib)
FIGDIR 	= Figures
FIGURES = $(wildcard $(FIGDIR)/*.png)
FIGURES += $(wildcard $(FIGDIR)/*.jpg)
FIGURES += $(wildcard $(FIGDIR)/*.gif)
FIGURES += $(wildcard $(FIGDIR)/*.fig)
FIGURES += $(wildcard $(FIGDIR)/*.dia)
FIGURES += $(wildcard $(FIGDIR)/*.svg)
CONVERT_FIGURES = $(patsubst %.fig,%.eps,$(wildcard $(FIGDIR)/*.fig))
CONVERT_FIGURES += $(patsubst %.fig,%.pdf,$(wildcard $(FIGDIR)/*.fig))
CONVERT_FIGURES += $(patsubst %.latexfig,%.eps,$(wildcard $(FIGDIR)/*.latexfig))
CONVERT_FIGURES += $(patsubst %.dia,%.eps,$(wildcard $(FIGDIR)/*.dia))
CONVERT_FIGURES += $(patsubst %.dia,%.pdf,$(wildcard $(FIGDIR)/*.dia))
CONVERT_FIGURES += $(patsubst %.svg,%.eps,$(wildcard $(FIGDIR)/*.svg))
CONVERT_FIGURES += $(patsubst %.svg,%.pdf,$(wildcard $(FIGDIR)/*.svg))
CONVERT_FIGURES += $(patsubst %.jpg,%.eps,$(wildcard $(FIGDIR)/*.jpg))
CONVERT_FIGURES += $(patsubst %.jpg,%.pdf,$(wildcard $(FIGDIR)/*.jpg))
CONVERT_FIGURES += $(patsubst %.gif,%.eps,$(wildcard $(FIGDIR)/*.gif))
CONVERT_FIGURES += $(patsubst %.gif,%.png,$(wildcard $(FIGDIR)/*.gif))
CONVERT_FIGURES += $(patsubst %.gif,%.pdf,$(wildcard $(FIGDIR)/*.gif))
SOURCES = $(filter-out $(wildcard toc_*.tex) tochtml.tex \
	chapens.tex chap.tex diapos.tex diaposnb.tex html.tex, \
	$(wildcard *.tex))
ifneq ($(VERBOSE),)
LATEXREDIRECT = 
else
LATEXREDIRECT = > /dev/null
VOIRCOMM = @
endif

include Makefile-configure

voirvar :
	@echo Makefile : BASEDIAPOS = $(BASEDIAPOS)
	@echo Makefile : BASECHAP = $(BASECHAP)
	@echo Makefile : BASECHAPENS = $(BASECHAPENS)
	@echo Makefile : BASEHTML = $(BASEHTML)
	@echo Makefile : SOURCES = $(SOURCES)
	@echo Makefile : BIBS = $(BIBS)
	@echo Makefile : FIGURES = $(FIGURES)
	@echo Makefile : LATEXREDIRECT = $(LATEXREDIRECT)
	@echo Makefile : VOIRCOMM = $(VOIRCOMM)
	@echo Makefile : CONVERT_FIGURES = $(CONVERT_FIGURES)
	make post-voirvar

all:	pre-all diapos deuxpp quatrepp chap chapens html post-all

tout:	all

figures : $(CONVERT_FIGURES)

diapos:	lightclean pre-diapos figures  $(BASEDIAPOS).pdf post-diapos

chap:	lightclean pre-chap figures $(BASECHAP).pdf post-chap

chapens: lightclean pre-chapens figures $(BASECHAPENS).pdf post-chapens

deuxpp:	pre-deuxpp lightclean figures $(BASEDIAPOSNB).pdf \
	$(BASEDIAPOSNB)-2pp.pdf post-deuxpp

quatrepp: pre-quatrepp lightclean figures $(BASEDIAPOSNB).pdf \
	$(BASEDIAPOSNB)-4pp.pdf post-quatrepp

cleanall: pre-cleanall clean
	$(VOIRCOMM)rm -f *.pdf *.tgz *.dvi
	$(VOIRCOMM)rm -rf $(ENLIGNE)
	$(VOIRCOMM)[[ -d ConfigTex4ht ]] && make -C ConfigTex4ht cleanall ; true

clean:	pre-clean lightclean
	$(VOIRCOMM)rm -f $(FIGDIR)/[!_]*.eps $(FIGDIR)/[!_]*.pdf \
		$(FIGDIR)/[!_]*.png $(FIGDIR)/*.bak

lightclean: pre-lightclean
	$(VOIRCOMM)rm -f *.log *.aux *.bbl *.blg *.toc *~ *.bak *.out \
		*.idx *.ind *.ilg *.tmp *.lof *.lot tochtml.tex toc_*.tex \
		*_tmp.ps *.css *.idv *.lg *.xref *ppm *4ct *4tc *.fls \
		*-js.js *-js.tex *.html *.4dx *.4ix *.5dx *.delme \
		$(FIGDIR)/*.fig.bak
	$(VOIRCOMM)rm -rf Tidy InsertionTitre CarAccentISO

%.dvi: $(SOURCES)
	$(VOIRCOMM)latex -file-line-error $*.tex $(LATEXREDIRECT)
	$(VOIRCOMM)if grep -q '\\bibliography{' $(SOURCES); \
		then bibtex $* $(LATEXREDIRECT); true \
		else true; \
	fi
	$(VOIRCOMM)-make other_bibtex BIBTEXFILE=$*
	$(VOIRCOMM)latex -file-line-error $*.tex $(LATEXREDIRECT)
	$(VOIRCOMM)if grep -q '\\slideprintindex' $(SOURCES); \
		then makeindex -L -s index.isty -t index.ilg $* \
			$(LATEXREDIRECT); true \
		else true; \
	fi
	$(VOIRCOMM)-make other_makeindex MAKEINDEXFILE=$*
	$(VOIRCOMM)[[ -a $*.bbl || -a $*.ilg ]] && \
		(latex -file-line-error $*.tex $(LATEXREDIRECT)); true
	$(VOIRCOMM)latex -file-line-error $*.tex $(LATEXREDIRECT)
	$(VOIRCOMM)for i in toc_*.tex ; \
	do\
		cp $$i tmp.tex ; \
		uniq tmp.tex > $$i ; \
		rm -f tmp.tex ; \
	done
	$(VOIRCOMM)latex -file-line-error $*.tex $(LATEXREDIRECT)

%.pdf:	%.dvi
	$(VOIRCOMM)dvipdfmx -p a4 -z9 -o $*.pdf $*.dvi

%-2pp.pdf: %.pdf
	$(VOIRCOMM)pdfnup --nup '1x2' --papersize '{21cm,29.7cm}' \
		--no-landscape --scale 0.9 --frame true --suffix '2pp' $*.pdf

%-4pp.pdf: %.pdf
	$(VOIRCOMM)pdfnup --nup '2x2' --papersize '{21cm,29.7cm}' \
		--landscape --scale 0.9 --frame true --suffix '4pp' $*.pdf

%.eps: %.fig
	$(VOIRCOMM)fig2dev -L eps $< > $@

%.pdf: %.fig
	$(VOIRCOMM)fig2dev -L pdf $< > $@

%.eps: %.latexfig
	$(VOIRCOMM)(cd ./Figures ; ./transform_latexfig.sh $<)

%.eps: %.dia
	$(VOIRCOMM)dia -t eps $< -e $@

%.pdf: %.dia
	$(VOIRCOMM)dia -t pdf $< -e $@

%.eps: %.svg
	$(VOIRCOMM)inkscape --export-eps=$@ $<

%.pdf: %.svg
	$(VOIRCOMM)inkscape --export-pdf=$@ $<

html:	lightclean pre-html figures $(ENLIGNE)/index.html post-html

$(ENLIGNE)/index.html: $(SOURCES)
	$(VOIRCOMM)rm -rf $(ENLIGNE)
	$(VOIRCOMM)install -d $(ENLIGNE)/ \
		$(ENLIGNE)/Images \
		$(ENLIGNE)/Figures Tidy InsertionTitre CarAccentISO
	$(VOIRCOMM)ls Images/*.gif > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && \
		 cp Images/*.gif $(ENLIGNE)/Images;\
		 true;
	$(VOIRCOMM)ls Images/*.png > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && \
		 cp Images/*.png $(ENLIGNE)/Images;\
		 true;
	$(VOIRCOMM)if grep -q '\\bibliography{' $(SOURCES); \
		then touch bib.delme; \
		else true; \
	fi
	$(VOIRCOMM)if grep -q '\\slideprintindex' $(SOURCES); \
		then touch index.delme; \
		else true; \
	fi
	$(VOIRCOMM)${LATEXTEX4HT} ${BASEHTML} "ConfigTex4ht/tex4ht.cfg" \
		$(LATEXREDIRECT)
	$(VOIRCOMM)[[ -a bib.delme && ! -a index.delme ]] && \
		(${LATEXTEX4HT} ${BASEHTML} \
			"ConfigTex4ht/tex4ht.cfg" $(LATEXREDIRECT)); true
	$(VOIRCOMM)[[ -a index.delme ]] && \
		(${LATEXTEX4HT} ${BASEHTML} \
			"ConfigTex4ht/tex4ht.cfg" $(LATEXREDIRECT); \
		 ${LATEXTEX4HT} ${BASEHTML} \
			"ConfigTex4ht/tex4ht.cfg" $(LATEXREDIRECT)); true
	$(VOIRCOMM)[[ -a bib.delme ]] && \
		(bibtex  ${BASEHTML} $(LATEXREDIRECT)); true
	$(VOIRCOMM)[[ -a index.delme ]] && \
		(${MAKEINDEXTEX4HT} ${BASEHTML} $(LATEXREDIRECT)); true
	$(VOIRCOMM)${LATEXTEX4HT} ${BASEHTML} \
		"ConfigTex4ht/tex4ht.cfg" $(LATEXREDIRECT)
	$(VOIRCOMM)${LATEXTEX4HT} ${BASEHTML} \
			"ConfigTex4ht/tex4ht.cfg" $(LATEXREDIRECT)
	$(VOIRCOMM)[[ -a index.delme ]] && \
		(${LATEXTEX4HT} ${BASEHTML} \
			"ConfigTex4ht/tex4ht.cfg" $(LATEXREDIRECT)); true
	$(VOIRCOMM)${TEX4HT} ${BASEHTML} $(LATEXREDIRECT)
	$(VOIRCOMM)-mv *.html Tidy
	$(VOIRCOMM)-mv *.css Tidy
	$(VOIRCOMM)ls *.png > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && mv -f *.png Tidy; true;
	$(VOIRCOMM)ls *.gif > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && mv -f *.gif Tidy; true;
	$(VOIRCOMM)cp ConfigTex4ht/tidy.cfg Tidy
	$(VOIRCOMM)(cd Tidy ; \
	for i in *.html ;\
	do\
		tidy -config tidy.cfg -f tidy.log -o \
			../InsertionTitre/$$i $$i ; \
		if (( $$? == 1 )) ; \
			then \
				echo \\n\\n\*\*\* file $$i >> \
					tidy_all.log ; \
				cat tidy.log >> tidy_all.log ; \
		fi ; \
		if (( $$? == 2 )) ; \
			then \
				echo tidy: \*\*\* Error: file $$i ; \
		fi ; \
	done ; \
	cd ..)
	$(VOIRCOMM)rm Tidy/tidy.cfg
	$(VOIRCOMM)cp Tidy/*.css InsertionTitre
	$(VOIRCOMM)ls Tidy/*.png > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp -f Tidy/*.png InsertionTitre; true;
	$(VOIRCOMM)ls Tidy/*.gif > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp -f Tidy/*.gif InsertionTitre; true;
	$(VOIRCOMM)cp Tidy/*.css $(ENLIGNE)
	$(VOIRCOMM)ls Tidy/*.png > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp Tidy/*.png $(ENLIGNE); true;
	$(VOIRCOMM)ls Tidy/*.gif > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp Tidy/*.gif $(ENLIGNE); true;
	$(VOIRCOMM)ls Tidy/${BASEHTML}*.html > /dev/null 2> /dev/null; \
		EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp Tidy/${BASEHTML}*.html \
			$(ENLIGNE); true;
	$(VOIRCOMM)-cp -f Tidy/${BASEHTML}*.html $(ENLIGNE)
	$(VOIRCOMM)-recode -d ISO-8859-15..HTML_4.0 \
		$(ENLIGNE)/${BASEHTML}*.html
	$(VOIRCOMM)ln -f -s ${BASEHTML}.html InsertionTitre/index.html
	$(VOIRCOMM)make -C ConfigTex4ht TITRE=${TITRE} \
		LONGUEUR_MENU=${LONGUEUR_MENU} \
		ORIGINE=InsertionTitre INTERMEDIAIRE=CarAccentISO \
		FINAL=$(ENLIGNE)
	$(VOIRCOMM)recode -d ISO-8859-15..HTML_4.0 \
		InsertionTitre/${BASEHTML}.css
	$(VOIRCOMM)cp InsertionTitre/${BASEHTML}.css $(ENLIGNE)
	$(VOIRCOMM)-rm $(ENLIGNE)/${BASEHTML}.html
	$(VOIRCOMM)cp ConfigTex4ht/*.css $(ENLIGNE)
	$(VOIRCOMM)cp ConfigTex4ht/*.html $(ENLIGNE)
	$(VOIRCOMM)ls Figures/*.png > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp Figures/*.png \
			$(ENLIGNE)/Figures; true;
