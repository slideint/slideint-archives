% -*-latex-*-
% formatter le titre et l'ins�rer dans le menu comme page d'accueil
\renewcommand{\maketitle}[1][notallinonepage]{%
  \HCode{<center>}%
  {\Huge\bfseries \@title}%
  \HCode{<p>}%
  {\Large\bfseries \@author\ifthenelse{\equal{\theauthorother}{\empty}}{}{, \@authorother}%
    \HCode{<p>}%
    \largepropriologo%
    \HCode{<p>}%
    \@address
    \HCode{<p>}%
    \@date
    \HCode{<p>}%
    {\ifthenelse{\equal{\thenumversion}{\empty}}
      {}
      {\small\thenumversion}}}
  \HCode{</center>}%
  \titlewonum{\@title}%
  \titlesecslide{~}\titlesubsecslide{~}\titlesubsubsecslide{~}%
  \slidenumero{\thetocsection}%
  \slidefilename{index.html}%
  \NextFile{\theslidefilename}%
  \titlesecslide{\iflanguage{english}{Home}{Accueil}}%
  \write\tochtml{SECTION}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{100}%
  \write\tochtml{100}%
  \write\tochtml{\thetitlesecslide}%
  \write\tochtml{\theslidefilename}%
  \ifthenelse{\equal{#1}{notallinonepage}}{\nextlinkAccueil}{\HCode{<br><p><hr width="60\%">}}\newpage}
% utiliser slidegraphics � la place de includegraphics
\renewcommand{\largepropriologo}{\ifthenelse{\equal{\thepropriologofile}{\empty}}{}{\includegraphics[scale=1.0]{\thepropriologofile}}}
% mettre le lien suivant dans la page d'accueil dans TeX4ht
\newcommand{\nextlinkAccueil}{%
   \HCode{<p>}~~~%
   \HCode{<a href="1.html"><span class="crosslink">}~\iflanguage{english}{next}{suivant}~\HCode{</span></a>}%
   \HCode{<p><hr width="60\%"><p>}}
% si on veut tous les crosslinks, pensez � mettre \empty
\Configure{crosslinks+} {} {}%
    {}%
    {\HCode{<span class="basdepage">}%
     \theauthor, \ifthenelse{\equal{\theauthorother}{\empty}}{}{\theauthorother,} \thepropriotext, \theaddress, \thedate%
     \HCode{<img align="right" src="./Images/angle_titre.gif" width="20" height="17" alt="">}}
% pour ne pas perdre les alignements avec les espaces, en faire une image
\ConfigureEnv{verbatim}{~\\\begin{large}\Picture*[verbatim]{}}%
	{\EndPicture\end{large}}{}{}
% pour ne pas perdre les alignements avec les espaces, en faire une image
\ConfigureEnv{Verbatim}{~\\\begin{large}\Picture*[Verbatim]{}}%
	{\EndPicture\end{large}}{}{}
% pour ne pas perdre les alignements avec les espaces, en faire une image
\ConfigureEnv{tabbing}{~\\\begin{large}\Picture*[tabbing]{}}%
	{\EndPicture\end{large}}{}{}
% environnement indiquant que le contenu doit �tre mis dans une image
\ConfigureEnv{slidecontentinimage}{~\\\begin{large}\Picture*[inimage]{}}%
	{\EndPicture\end{large}}{}{}
% ins�rer ``Index'' dans le menu
% pas de titre avec section* puisque d�j� fait par /printindex
\renewcommand{\slideprintindex}{
  \addtocounter{tocsection}{1}%
  \slidenumero{\thetocsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \write\tochtml{section}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{100}%
  \write\tochtml{100}%
  \write\tochtml{Index}%
  \write\tochtml{\theslidefilename}%
  \printindex
}
% Attention ! le r�pertoire contenant les images n'est pas un sous-r�pertoire.
% ????? \renewcommand{\slideimage}[2]{\HCode{<img src="}./#2.png\HCode{">}}
% section dans les notes (sans num�rotation) mais dans toc
% dans un environnement pour permettre � TeX4ht de mettre les liens
% suivants et pr�c�dents
\renewenvironment{secnote}[2][slidetitletoc]{
  \titlesecslide{~}\titlesubsecslide{~}\titlesubsubsecslide{~}%
  \closeout\tocsectionhtml%
  \addtocounter{tocsection}{1}%
  \slidenumero{\thetocsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \centering\protect\section*{#2}%
  \titlesecslide{#2}%
  \write\tochtml{section}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{100}%
  \write\tochtml{100}%
  \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tochtml{#2}}{\write\tochtml{#1}}%
  \write\tochtml{\theslidefilename}}%
		 {\openout\tocsectionhtml=toc_sec_\thetocsection%
		  \prevnextlinks}
% section dans les notes (sans num�rotation) mais dans toc
% dans un environnement pour permettre � TeX4ht de mettre les liens
% suivants et pr�c�dents
\renewenvironment{subsecnote}[2][slidetitletoc]{
  \titlesubsecslide{~}\titlesubsubsecslide{~}%
  \closeout\tocsubsectionhtml%
  \addtocounter{tocsubsection}{1}%
  \slidenumero{\thetocsection.\thetocsubsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \centering\protect\subsection*{#2}%
  \titlesubsecslide{#2}%
  \write\tocsectionhtml{\noexpand\HCode\noexpand{<a href=\noexpand"\theslidefilename">}#2\noexpand\HCode\noexpand{</a>\noexpand}}%
  \write\tocsectionhtml{\noexpand\\}%
  \write\tochtml{subsection}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{\thetocsubsection}%
  \write\tochtml{100}%
  \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tochtml{#2}}{\write\tochtml{#1}}%
  \write\tochtml{\theslidefilename}}%
		 {\openout\tocsubsectionhtml=toc_sub_sec_\thetocsubsection%
		  \prevnextlinks}
% section dans les notes (sans num�rotation) mais dans toc
% dans un environnement pour permettre � TeX4ht de mettre les liens
% suivants et pr�c�dents
\renewenvironment{subsubsecnote}[2][slidetitletoc]{%
  \titlesubsubsecslide{~}%
  \addtocounter{tocsubsubsection}{1}%
  \slidenumero{\thetocsection.\thetocsubsection.\thetocsubsubsection}%
  \slidefilename{\theslidenumero.html}%
  \NextFile{\theslidefilename}%
  \centering\protect\subsubsection*{#2}%
  \titlesubsubsecslide{#2}%
  \write\tocsubsectionhtml{\noexpand\HCode\noexpand{<a href=\noexpand"\theslidefilename">}#2\noexpand\HCode\noexpand{</a>\noexpand}}%
  \write\tocsubsectionhtml{\noexpand\\}%
  \write\tochtml{subsubsection}%
  \write\tochtml{\thetocsection}%
  \write\tochtml{\thetocsubsection}%
  \write\tochtml{\thetocsubsubsection}%
  \ifthenelse{\equal{#1}{slidetitletoc}}{\write\tochtml{#2}}{\write\tochtml{#1}}%
  \write\tochtml{\theslidefilename}}%
		 {\prevnextlinks}
% no table of content in the HTML version
\renewcommand{\slidetableofcontents}{}
\endinput
