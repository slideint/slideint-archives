Foire Aux Questions du projet :
===============================

NB: dans le texte qui suit, la commande "make" correspond à la commande
    "slideint" utilisée par l'utilisateur ayant installé le paquetage
    du même nom.

Comprendre l'arborescence :
---------------------------

- Question : quelle est la différence entre les fichiers ./Makefile et
  ./Ressources/Makefile ?
- Réponse : le fichier ./Makefile est celui qui est utilisé pour
  toutes les commandes "make" de l'utilisateur : il s'ensuit que
  toutes les commandes doivent être lancé à partir de répertoire, ET
  pas dans les sous-répertoires du contenu. Quant aux fichiers
  ./Ressources/Makefile, c'est le fichier générique de tous les
  commandes "make" pour les chapitres. D'ailleurs, le ./Makefile
  crée des liens symboliques dans les chapitres avant d'appeler les
  commandes "make". C'est donc le fichier qu'il faut adapter pour des
  besoins particuliers comme le traitement de QCM où d'extraction de code
  avant les compilations LaTeX.

- Question : à quoi sert le fichier ./Makefile-configure ?
- Réponse : ce fichier est le fichier de configuration utilisé par le
  script Perl pour connaître les regroupements de chapitres : tous les
  cours, tous les énoncés de TP, tous les corrigés de TP. Les
  "regroupements" sont écrits comme des variables en syntaxe "make",
  car ce fichier est aussi inclus en début du fichier ./Makefile. Il
  est à noter les noms de ces variables sont libres, en plus de leur
  contenu, excepté pour les variables "POLY" et "POLYENS",
  respectivement pour la description du contenu des polycopiés
  étudiant et enseignant.

- Question : qu'est-ce que le répertoire ./Documents ? Pourquoi n'est-il
  jamais cité dans le fichier ./Makefile-configure ? Pourquoi le
  fichier ./Makefile fait-il un "svn export" de ce répertoire ?
- Réponse : le répertoire ./Documents ne fait pas l'objet d'un
  traitement pour les générations PostScript, PDF ou HTML. Ce
  répertoire contient tout le contenu à ne pas traiter, si ce n'est en
  faire une copie par "svn export" dans ./Public. La copie s'effectue
  pas "svn export" pour ne pas copier les répertoires ".svn" du dépôt
  SVN.

- Question : Il semble un peu étrange de tout lancer d'en haut de
  l'arborescence. Pourquoi ne pas avoir un makefile dans chaque
  répertoire qui incluent des make génériques présents en haut de
  l'arborescence et le make du haut lance récursivement les make dans
  les sous répertoires.
- Réponse : Cette contrainte permet de simplifier considérablement
  l'organisation. Par exemple, si le processus (générique) de
  construction d'une cible change, un seul fichier
  ("./Ressources/Makefile") est à changer. Autre exemple, la mise à jour
  de "./Ressources/slideint.sty" ou autre fichier de configuration est
  aussi immédiate. Le placement de fichiers makefile dans les
  répertoires qui inclut des fichiers présents dans le haut de
  l'arborescence possède les inconvénients suivant : 1) le répertoire
  racine est pollué par tous les fichiers inclus ; 2) ces fichiers
  makefile dans les répertoires de contenu doivent posséder quelques
  informations de configuration spécifiques au chapitre, par exemple
  le nom logique du chapitre, et sont donc non égaux en terme de
  contenu ; 3) les répertoires de contenu sont pollués par ces
  fichiers makefile. La solution proposée ici passe sans problème à
  l'échelle, jusqu'à présent (une quarantaine de chapitre).

Trouver/utiliser/comprendre les cibles "make" :
-----------------------------------------------

- Question : où trouver la liste des cibles "make" appelables ?
- Réponse : lancer "make", et lire le message qui s'affiche en lisant
  les questions peut-être les réponses aux questions qui suivent.

- Question : quels exemples peut-on donner pour des premiers essais
pertinents de la commande "make" ?
- Réponse : commencer par essayer ceci :
  - "make chap LIST=C1"		: format article, version étudiant, de C1
  - "make chapens LIST=C3"	: format article, version enseignant, de C3
  - "make html LIST=C1,C2"	: format HTML, de C1 et C2
  - "make chap LIST=Poly"	: polycopié, version étudiant
  - "make slides"		: tous les transparents, tous les répertoires
  - "make cleanall LIST=C1"	: nettoyage complet du répertoire C1
  - "make public LIST=C1"	: mise dans Public transparents + HTML, de C1
  Il est à noter que que l'on ne s'occupe pas d'indiquer où sont les
  répertoires C1, C3, Poly, etc.

- Question : pourquoi aucun affichage en sortie standard des résultats
des commandes LaTeX et TeX4ht ?
- Réponse : par défaut, les affichages en sortie standard des
commandes LaTeX et TeX4ht sont redirigées vers /dev/null. Vous pouvez
dans ce cas vous reporter aux détails des .log. Pour supprimer la
redirection, affectez la macro VERBOSE à une valeur non nulle, comme
ceci par exemple : $ make chap LIST=C1 VERBOSE=yes

- Question : comment générer le polycopié ?
- Réponse : il y a deux versions du polycopié : la version pour les
  étudiants (voir la variable "POLY" dans le fichier
  "./Makefile-configure") et la version pour les enseignants (voir la
  variable "POLYENS" dans le même fichier).

- Question : comment générer le site web ?
- Réponse : il est possible de générer le site web chapitre par
  chapitre (commande "make public LIST=[liste des chapitres]) ou dans
  sa totalité (commande "make public"). La cible "public" est incluse
  dans la cible all (commande "make all").

- Question : à quoi servent les cibles *public dans les fichiers
  Makefile ?
- Réponse : Les résultats des compilations sont dispersés dans les
  répertoires de contenu. Les cibles *public ont pour objectif de
  rassembler ces éléments dans l'arborescence distincte ./Public pour
  préparer les phases de publication sur etna ou sur Moodle. Ce
  répertoire ./Public présent dans le dépôt SVN DOIT rester vide dans le
  dépôt. Ce n'est donc qu'un conteneur du contenu provisoire à
  transférer par scp sur les sites de publication.

- Question : où mettre les figures pour les chapitres ?
- Réponse : la convention est de les mettre dans un sous-répertoire
  Figures du chapitre. Un lien symbolique Figures/Makefile est
  automatiquement ajouté. Les commandes LaTeX d'inclusion de fichiers
  seront donc de la forme
  "\includegraphics[scale=votreEchelle]{Figures/nomFichierFigureSansExtension}".

Compléter/modifier le contenu ou la configuration :
---------------------------------------------------

- Question : comment ajouter un nouveau chapitre dans le cours ?
- Réponse : procéder avec les étapes qui suivent :
  1- créer un nouveau répertoire, disons NouvTP, dans la catégorie/liste
  voulue, disons ./EnonceTP,
  2- modifier le fichier ./Makefile-configure en ajoutant ce nouveau
     répertoire à la variable EnonceTP, et sans doute aux variables POLY
     et POLYENS (pour insérer le nouveau chapitre dans les polycopiés),
  3- ajouter le contenu (un fichier contenu.tex, etc.) dans
     ./EnonceTP/NouvTP,
  Ce nouveau chapitre est compilable par "make cible LIST=NouvTP", avec
  cible = slides, chap, chapens, html, tout, cleanall...

- Question : que dois-je faire pour configurer le polycopié global ?
- Réponse : Modifier les fichiers Poly/debut.tex et Poly/debut-ens.tex
  en y adaptant les lignes suivantes :
     \slidetitle{Polycopié\\version étudiants}
     \slideauthor{Denis Conan}
     \mainlogofile{Images/mainlogo}
     \slidepropriologofile{Images/logo}
     \slidepropriotext{GET~/~INT~/~INF}
     \slideaddress{Télécom INT~---~1ère année}
     \slidedate{Un jour de 2005}
  En outre, modifier les variables POLY et POLYENS dans
  ./Makefile-configure pour définir les chapitres sélectionné pour ces
  2 polycopiés. Enfin, le logo principal du cours doit être stocké
  dans le fichier Ressources/Images/mainlogo.eps (si vous l'avez sous
  une autre dénomination, à vous d'utiliser convert pour le mettre au
  format eps ! Ce logo peut donc être différent des logos des chapitres.
 
- Question : comment ajouter l'utilisation d'un nouveau package LaTeX ?
- Réponse : les fichiers ./Ressources/Common-tex/configuration.tex et
  ./Poly/configuration.tex sont les meilleurs endroits. Un cas
  particulier, par exemple en cas de conflits entre package, pourrait
  consister à faire l'ajout dans les fichiers
  Ressources/Common-tex/common-*.tex ; mais, demandez avant conseil !

- Question : Comment ajouter/gérer des bibliographies locales à un
  chapitre ?
- Réponse : les bibliographies locales à un chapitre sont à ajouter
  dans le chapitre. Puis, ajouter les appels qu'il faut à bibtex dans
  ./Ressources/Makefile, avec un tiret ("-") devant pour les chapitres
  qui n'ont pas de bibliographie.
  Pour une bibliographie globale, voire la question suivante.

- Question : Comment ajouter/gérer une bibliographie globale au cours ?
- Réponse : il est dans ce cas préférable de créer un nouveau
  chapitre, c'est-à-dire de :
  1- créer un répertoire Bibliographie dans le répertoire ./Cours,
  2- modifier le fichier ./Makefile-configure en ajoutant ce nouveau
     répertoire à la variable Cours, et sans doute aux variables POLY
     et POLYENS (pour insérer la bibliographie dans les polycopiés),
  3- ajouter un fichier contenu.tex qui fait appel à la bibliographie,
  4- ajouter le fichier .bib de la bibliographie dans le répertoire du
     chapitre.
  5- modifier le fichier ./Makefile pour ajouter l'appel à bibtex,
     comme dans le cas de l'ajout d'une bibliographie locale. Pour une
     bibliographie locale à un chapitre, voire la question précédente.

- Question : comment corriger/introduire un correctif dans un
  chapitre, par exemple le contenu du chapitre "C1"?
- Réponse : toujours à partir de la racine (c'est un conseil, pas une
  obligation), trouver où se trouve le chapitre dans l'arborescence
  (Unix et "./Makefile-configure" sont des amis), et éditer les
  fichiers de contenu, ici "./Cours/C1/*.tex".

- Question : le source des supports de cours ayant vocation à être
  public, ne faudrait-il pas mettre les corrigés des TD et TP dans une
  arborescence ?
- Réponse : si l'on met tout (cours, sujets et corrigés) dans la même
  arborescence, le projet picoforge doit être "privé" et c'est la
  phase de publication qui fait le tri entre ce qui est publié sur
  moodle (tout mettre car il est possible de contrôler l'accès
  [visible ou non]), et ce qui est publié sur etna (ne pas mettre les
  corrigés). En revanche, séparer les corrigés, c'est-à-dire ne pas
  les mettre dans le même projet, permet de mettre "public" le projet
  picoforge contenant le cours et les sujets.

- Question : Les numéros de cours, TD, TP sont-ils impératifs dans les
  noms de répertoire ? ou est-il possible sans perturber le makefile
  de choisir des noms par thème (style C-Exceptions,
  CTD-Exceptions...) ?
- Réponse : Les noms des listes (Cours, ETP, etc.) sont configurables
  et les noms des items dans ces listes, donc des répertoires. Tout
  cela est à faire en correspondance avec le fichier ./Makefile-configure.

- Question : Et si un chapitre demande un Makefile spécifique, par
  exemple pour un glossaire local présenté de manière spécifique ou
  une bibliographie avec un nom de fichier spécifique ?
- Réponse : rien n'empêche d'ajouter un Makefile dans le dit
  chapitre. C'est ce Makefile qui sera utilisé, car la création du
  lien symbolique sur le ./Ressources/Makefile échouera. Ce principe est
  d'ailleurs utilisable pour tous les fichiers et répertoire de
  Ressources. Par conséquent, il est possible de surcharger les
  configurations et les procédures de traitement génériques.

- Question : Comment faire en sorte que des figures
  EncapsulatedPostScript ou PNG ne soient pas effacées par les cibles
  *clean* des makefile ?
- Réponse : la convention est de préfixer ces fichiers avec
  l'extension "_".

- Question : Toutes les version papier des chapitres possèdent deux
  versions, la première pour les étudiants (chap), l'autre pour les
  enseignants (chapens). Comment fait-on la différence au niveau du
  contenu ?
- Réponse : en utilisant la commande \opt, par exemple
  \opt{enseignant} pour réserver un contenu pour les enseignants, de
  la feuille de style LaTeX optional.sty. Pour les options
  disponibles, voire les fichiers commun de
  Ressources/Common-tex/. Pour des exemples d'utilisation, voire le
  projet picoforge initux. Ne vous prenez pas la tête avec cela, mais
  allez voir ceux qui sont déjà aguerris à ces manipulations ;-) .

- Question : Y-a-t-il des restrictions sur les noms des fichiers LaTeX ?
- Réponse : Oui. D'une part, le fichier de départ est toujours
  contenu.tex. D'autre part, en supposant que Chap le nom du
  répertoire du chapitre, les noms suivants sont interdits :
  chap-slides.tex, chap-html.tex, chap-notes.tex, chap-notes-ens.tex. 
