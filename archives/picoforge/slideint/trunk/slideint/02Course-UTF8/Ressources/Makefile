############################################################################
#									   #
# Ce makefile doit imperativement etre invoque en definissant les macros : #
#     C : le radical du nom du chapitre/cours pour nommage des fichiers    #
#     DIRC : le chemin d'acces au repertoire depuis le top 		   #
#     ABSDIRC : le chemin d'acces absolu au repertoire			   #
#									   #
# Des liens sur ce makefile sont crees par le Makfile principal dans le	   #
# repertoire de chaque chapitre utilise					   #
#									   #
############################################################################

SHELL=/bin/bash

# inclusion des elements specifiques de ce cours
include Makefile-configure
include ../../Makefile-configure

.SUFFIXES : # Delete the default suffixes

SHELL   = /bin/bash
LATEX	= /usr/bin/latex
LATEXTEX4HT = ConfigTex4ht/latextex4ht.sh
MAKEINDEXTEX4HT = ConfigTex4ht/makeindextex4ht.sh
TEX4HT = ConfigTex4ht/tex4ht.sh

OTHER_DEPENDENCIES +=
BIBS	= $(wildcard *.bib)
BASEHTML= $(C)-html
PUBLICDIR =  $(BASEDIR)/Public
FIGDIR 	= Figures
FIGURES = $(wildcard $(FIGDIR)/*.png)
FIGURES += $(wildcard $(FIGDIR)/*.jpg)
FIGURES += $(wildcard $(FIGDIR)/*.gif)
FIGURES += $(wildcard $(FIGDIR)/*.fig)
FIGURES += $(wildcard $(FIGDIR)/*.dia)
FIGURES += $(wildcard $(FIGDIR)/*.svg)
CONVERT_FIGURES = $(patsubst %.fig,%.eps,$(wildcard $(FIGDIR)/*.fig))
CONVERT_FIGURES += $(patsubst %.fig,%.pdf,$(wildcard $(FIGDIR)/*.fig))
CONVERT_FIGURES += $(patsubst %.latexfig,%.eps,$(wildcard $(FIGDIR)/*.latexfig))
CONVERT_FIGURES += $(patsubst %.dia,%.eps,$(wildcard $(FIGDIR)/*.dia))
CONVERT_FIGURES += $(patsubst %.dia,%.pdf,$(wildcard $(FIGDIR)/*.dia))
CONVERT_FIGURES += $(patsubst %.svg,%.eps,$(wildcard $(FIGDIR)/*.svg))
CONVERT_FIGURES += $(patsubst %.svg,%.pdf,$(wildcard $(FIGDIR)/*.svg))
CONVERT_FIGURES += $(patsubst %.jpg,%.eps,$(wildcard $(FIGDIR)/*.jpg))
CONVERT_FIGURES += $(patsubst %.jpg,%.pdf,$(wildcard $(FIGDIR)/*.jpg))
CONVERT_FIGURES += $(patsubst %.gif,%.eps,$(wildcard $(FIGDIR)/*.gif))
CONVERT_FIGURES += $(patsubst %.gif,%.png,$(wildcard $(FIGDIR)/*.gif))
CONVERT_FIGURES += $(patsubst %.gif,%.pdf,$(wildcard $(FIGDIR)/*.gif))
SOURCES = $(filter-out $(wildcard toc_*.tex) tochtml.tex, $(wildcard *.tex))
LASTCHAP= $(SOURCES) $(BIBS) $(FIGURES) $(OTHER_DEPENDENCIES) lastCHAP.date
NEEDCHAP= $(word 1,$(shell ls -1 --sort=t $(LASTCHAP) 2> /dev/null))
LASTCHAPENS= $(SOURCES) $(BIBS) $(FIGURES) $(OTHER_DEPENDENCIES) lastCHAPENS.date
NEEDCHAPENS= $(word 1,$(shell ls -1 --sort=t $(LASTCHAPENS) 2> /dev/null))
LASTSLIDES= $(SOURCES) $(BIBS) $(FIGURES) $(OTHER_DEPENDENCIES) lastSLIDES.date
NEEDSLIDES= $(word 1,$(shell ls -1 --sort=t $(LASTSLIDES) 2> /dev/null))
LASTHTML= $(SOURCES) $(BIBS) $(FIGURES) $(OTHER_DEPENDENCIES) lastHTML.date
NEEDHTML= $(word 1,$(shell ls -1 --sort=t $(LASTHTML) 2> /dev/null))

CONFIGTEX4HT = ConfigTex4ht
TEX4HTCFG = tex4ht.cfg
TIDYCFG = tidy.cfg
ifneq ($(VERBOSE),)
LATEXREDIRECT = 
else
LATEXREDIRECT = > /dev/null
VOIRCOMM = @
endif

# Attention ! ne pas mettre de déplacement de répertoire ("../") dans ces noms
TIDY = Tidy
INSERTION_TITRE = InsertionTitre
INTERMEDIAIRE = CarAccentUTF
ENLIGNE = Web
CECOURS  = $(ENLIGNE)/Cours

voirvar : pre-voirvar
	@echo Ressources/Makefile : SOURCES = $(SOURCES)
	@echo Ressources/Makefile : LASTCHAP = $(LASTCHAP)
	@echo Ressources/Makefile : NEEDCHAP = $(NEEDCHAP)
	@echo Ressources/Makefile : LASTCHAPENS = $(LASTCHAPENS)
	@echo Ressources/Makefile : NEEDCHAPENS = $(NEEDCHAPENS)
	@echo Ressources/Makefile : LASTSLIDES = $(LASTSLIDES)
	@echo Ressources/Makefile : NEEDSLIDES = $(NEEDSLIDES)
	@echo Ressources/Makefile : LASTHTML = $(LASTHTML)
	@echo Ressources/Makefile : NEEDHTML = $(NEEDHTML)
	@echo Ressources/Makefile : LATEXREDIRECT = $(LATEXREDIRECT)
	@echo Ressources/Makefile : VOIRCOMM = $(VOIRCOMM)

err:	pre-err
	$(VOIRCOMM)echo Il faut choisir une cible... et de toute façon 
	$(VOIRCOMM)echo ce makefile doit imperativement etre invoque
	$(VOIRCOMM)echo en definissant trois macros :
	$(VOIRCOMM)echo '               'C, DIRC et ABSDIRC 
	$(VOIRCOMM)echo RAPPEL : appeler make uniquement a partir de la racine
	$(VOIRCOMM)make post-err

all:	pre-all html clean diapos chap chapens 2pp 4pp post-all

figures :pre-figures $(CONVERT_FIGURES)

liens-ressources: pre-liens-ressources
	$(VOIRCOMM)[[ ! -e ConfigTex4ht ]] && \
		(ln -s  $(RESSOURCESDIR)/ConfigTex4ht .); true
	$(VOIRCOMM)[[ ! -e Images ]] && ln -s  $(RESSOURCESDIR)/Images .; true
	$(VOIRCOMM)[[ -e Figures ]] && \
		(ln -s $(RESSOURCESDIR)/transform_latexfig.sh \
			Figures/transform_latexfig.sh); true
	$(VOIRCOMM)[[ ! -e Makefile ]] && \
		(ln -s  $(RESSOURCESDIR)/Makefile .); true
	$(VOIRCOMM)[[ ! -e Makefile-configure ]] && \
		(ln -s  $(RESSOURCESDIR)/Makefile-configure .); true
	$(VOIRCOMM)[[ ! -e newslide.cls ]] && \
		(ln -s  $(RESSOURCESDIR)/newslide.cls .); true
	$(VOIRCOMM)[[ ! -e newslide.4ht ]] && \
		(ln -s  $(RESSOURCESDIR)/newslide.4ht .); true
	$(VOIRCOMM)[[ ! -e slideint.sty ]] && \
		(ln -s  $(RESSOURCESDIR)/slideint.sty .); true
	$(VOIRCOMM)[[ ! -e index.isty ]] && \
		(ln -s  $(RESSOURCESDIR)/index.isty .); true
	$(VOIRCOMM)[[ ! -e index.4ht.isty ]] && \
		(ln -s  $(RESSOURCESDIR)/index.4ht.isty .); true
	$(VOIRCOMM)[[ ! -e slideint.4ht ]] && \
		(ln -s  $(RESSOURCESDIR)/slideint.4ht .); true
	$(VOIRCOMM)[[ ! -e configuration.tex ]] && \
		(ln -s $(RESSOURCESDIR)/Common-tex/configuration.tex \
			configuration.tex); true
	$(VOIRCOMM)[[ ! -e $(C)-html.tex ]] && \
		(ln -s $(RESSOURCESDIR)/Common-tex/common-html.tex \
			$(C)-html.tex); true
	$(VOIRCOMM)[[ ! -e $(C)-diapos.tex ]] && \
		(ln -s $(RESSOURCESDIR)/Common-tex/common-diapos.tex \
			$(C)-diapos.tex); true
	$(VOIRCOMM)[[ ! -e $(C)-chap.tex ]] && \
		(ln -s $(RESSOURCESDIR)/Common-tex/common-chap.tex \
			$(C)-chap.tex); true
	$(VOIRCOMM)[[ ! -e $(C)-chapens.tex ]] && \
		(ln -s $(RESSOURCESDIR)/Common-tex/common-chapens.tex \
			$(C)-chapens.tex); true

#Cibles pour création des Postscript
chap: liens-ressources pre-chap figures
	make lightclean
ifneq ($(findstring $(REP),$(GENERATECHAP)),)
ifneq ($(NEEDCHAP),lastCHAP.date)
ifneq ($(findstring $(REP),$(GENERATEPS) $(GENERATEPDF)),)
	$(VOIRCOMM)latex $(C)-chap.tex $(LATEXREDIRECT)
	$(VOIRCOMM)if grep -q '\\bibliography{' $(SOURCES); \
		then bibtex $(C)-chap; true \
		else true; \
	fi
	$(VOIRCOMM)latex $(C)-chap.tex $(LATEXREDIRECT)
	$(VOIRCOMM)if grep -q '\\slideprintindex' $(SOURCES); \
		then makeindex -L -s index.isty -t index.ilg \
			$(C)-chap; true \
		else true; \
	fi
	$(VOIRCOMM)[[ -a $(C)-chap.bbl || -a $(C)-chap.ilg ]] && \
		(latex -file-line-error $(C)-chap.tex) $(LATEXREDIRECT); true
	$(VOIRCOMM)latex $(C)-chap.tex $(LATEXREDIRECT)
else
	$(VOIRCOMM)true
endif
ifneq ($(findstring $(REP),$(GENERATEPS)),)
	dvips -Pwww -o $(C)-chap.ps $(C)-chap.dvi
else
	$(VOIRCOMM)true
endif
ifneq ($(findstring $(REP),$(GENERATEPDF)),)
ifeq ($(PS2PDF_DVIPDFM),$(DVIPDFM))
	dvipdfmx -p a4 -z9 -o $(C)-chap.pdf $(C)-chap.dvi
else
	dvips -Pwww -o $(C)-chap.ps $(C)-chap.dvi
	ps2pdf -sPAPERSIZE=a4 -dPDFsettings=/prepress \
		$(C)-chap.ps $(C)-chap.pdf
endif
else
	$(VOIRCOMM)true
endif
	$(VOIRCOMM)touch lastCHAP.date
	$(VOIRCOMM)make post-chap
else
	$(VOIRCOMM)true
endif
else
	$(VOIRCOMM)true
endif

chapens: liens-ressources  pre-chapens figures
	make lightclean
ifneq ($(findstring $(REP),$(GENERATECHAPENS)),)
ifneq ($(NEEDCHAPENS),lastCHAPENS.date)
ifneq ($(findstring $(REP),$(GENERATEPS) $(GENERATEPDF)),)
	$(VOIRCOMM)latex $(C)-chapens.tex $(LATEXREDIRECT)
	$(VOIRCOMM)if grep -q '\\bibliography{' $(SOURCES); \
		then bibtex $(C)-chapens; true \
		else true; \
	fi
	$(VOIRCOMM)latex $(C)-chapens.tex $(LATEXREDIRECT)
	$(VOIRCOMM)if grep -q '\\slideprintindex' $(SOURCES); \
		then makeindex -L -s index.isty -t index.ilg $(C)-chapens; true \
		else true; \
	fi
	$(VOIRCOMM)[[ -a $(C)-chapens.bbl || -a $(C)-chapens.ilg ]] && \
		(latex -file-line-error $(C)-chapens.tex) $(LATEXREDIRECT); true
	$(VOIRCOMM)latex $(C)-chapens.tex $(LATEXREDIRECT)
else
	$(VOIRCOMM)true
endif
ifneq ($(findstring $(REP),$(GENERATEPS)),)
	dvips -Pwww -o $(C)-chapens.ps $(C)-chapens.dvi
endif
ifneq ($(findstring $(REP),$(GENERATEPDF)),)
ifeq ($(PS2PDF_DVIPDFM),$(DVIPDFM))
	dvipdfmx -p a4 -z9 -o $(C)-chapens.pdf $(C)-chapens.dvi
else
	dvips -Pwww -o $(C)-chapens.ps $(C)-chapens.dvi
	ps2pdf -sPAPERSIZE=a4 -dPDFsettings=/prepress \
		$(C)-chapens.ps $(C)-chapens.pdf
endif
endif
	$(VOIRCOMM)touch lastCHAPENS.date
	$(VOIRCOMM)make post-chapens
else
	$(VOIRCOMM)true
endif
else
	$(VOIRCOMM)true
endif

diapos:	liens-ressources pre-diapos figures
	make lightclean
ifneq ($(findstring $(REP),$(GENERATESLIDES)),)
ifneq ($(NEEDSLIDES),lastSLIDES.date)
ifneq ($(findstring $(REP),$(GENERATEPS) $(GENERATEPDF)),)
	$(VOIRCOMM)latex $(C)-diapos.tex $(LATEXREDIRECT)
	$(VOIRCOMM)if grep -q '\\bibliography{' $(SOURCES); \
		then bibtex $(C)-diapos; true \
		else true; \
	fi
	$(VOIRCOMM)latex $(C)-diapos.tex $(LATEXREDIRECT)
	$(VOIRCOMM)if grep -q '\\slideprintindex' $(SOURCES); \
		then makeindex -L -s index.isty -t index.ilg \
			$(C)-diapos; true \
		else true; \
	fi
	$(VOIRCOMM)[[ -a $(C)-diapos.bbl || -a $(C)-diapos.ilg ]] && \
		(latex -file-line-error $(C)-diapos.tex) $(LATEXREDIRECT); true
	$(VOIRCOMM)latex $(C)-diapos.tex $(LATEXREDIRECT)
else
	$(VOIRCOMM)true
endif
ifneq ($(findstring $(REP),$(GENERATEPS)),)
	dvips -Pwww -o $(C)-diapos.ps $(C)-diapos.dvi
endif
ifneq ($(findstring $(REP),$(GENERATEPDF)),)
ifeq ($(PS2PDF_DVIPDFM),$(DVIPDFM))
	dvipdfmx -p a4 -z9 -o $(C)-diapos.pdf $(C)-diapos.dvi
else
	dvips -Pwww -o $(C)-diapos.ps $(C)-diapos.dvi
	ps2pdf -sPAPERSIZE=a4 -dPDFsettings=/prepress \
		$(C)-diapos.ps $(C)-diapos.pdf
endif
endif
	$(VOIRCOMM)touch lastSLIDES.date
	$(VOIRCOMM)make post-diapos
else
	$(VOIRCOMM)true
endif
else
	@true
endif

html:	liens-ressources pre-html figures
	make lightclean
ifneq ($(findstring $(REP),$(GENERATEHTML)),)
ifneq ($(NEEDHTML),lastHTML.date)
	$(VOIRCOMM)rm -rf $(ENLIGNE)
	$(VOIRCOMM)install -d $(ENLIGNE) $(CECOURS) $(CECOURS)/Images \
		$(CECOURS)/Figures $(TIDY) $(INSERTION_TITRE) $(INTERMEDIAIRE)
	$(VOIRCOMM)ls Images/*.gif > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp Images/*.gif $(CECOURS)/Images; true;
	$(VOIRCOMM)ls Images/*.png > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp Images/*.png $(CECOURS)/Images; true;
	$(VOIRCOMM)if grep -q '\\bibliography{' $(SOURCES); \
		then touch bib.delme; \
		else true; \
	fi
	$(VOIRCOMM)if grep -q '\\slideprintindex' $(SOURCES); \
		then touch index.delme; \
		else true; \
	fi
	$(VOIRCOMM)${LATEXTEX4HT} ${BASEHTML} $(CONFIGTEX4HT)/$(TEX4HTCFG) \
		$(LATEXREDIRECT)
	$(VOIRCOMM)[[ -a bib.delme && ! -a index.delme ]] && \
		(${LATEXTEX4HT} ${BASEHTML} \
			$(CONFIGTEX4HT)/$(TEX4HTCFG) $(LATEXREDIRECT)); true
	$(VOIRCOMM)[[ -a index.delme ]] && \
		(${LATEXTEX4HT} ${BASEHTML} \
			$(CONFIGTEX4HT)/$(TEX4HTCFG) $(LATEXREDIRECT); \
		 ${LATEXTEX4HT} ${BASEHTML} \
			$(CONFIGTEX4HT)/$(TEX4HTCFG) $(LATEXREDIRECT)); true
	$(VOIRCOMM)[[ -a bib.delme ]] &&  bibtex  ${BASEHTML}; true
	$(VOIRCOMM)[[ -a index.delme ]] &&${MAKEINDEXTEX4HT} ${BASEHTML}; true
	$(VOIRCOMM)${LATEXTEX4HT} ${BASEHTML} $(CONFIGTEX4HT)/$(TEX4HTCFG) \
		$(LATEXREDIRECT)
	$(VOIRCOMM)${LATEXTEX4HT} ${BASEHTML} \
			$(CONFIGTEX4HT)/$(TEX4HTCFG) $(LATEXREDIRECT)
	$(VOIRCOMM)[[ -a index.delme ]] && \
		(${LATEXTEX4HT} ${BASEHTML} \
			$(CONFIGTEX4HT)/$(TEX4HTCFG) $(LATEXREDIRECT)); true
	$(VOIRCOMM)${TEX4HT} ${BASEHTML} $(LATEXREDIRECT)
	$(VOIRCOMM)-mv -f *.html $(TIDY)
	$(VOIRCOMM)-mv -f *.css $(TIDY)
	$(VOIRCOMM)ls *.png > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && mv -f *.png $(TIDY); true;
	$(VOIRCOMM)ls *.gif > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && mv -f *.gif $(TIDY); true;
	$(VOIRCOMM)cp $(CONFIGTEX4HT)/$(TIDYCFG) $(TIDY)
	$(VOIRCOMM)(cd $(TIDY) ; \
	for i in *.html ;\
	do \
		tidy -config $(TIDYCFG) -f tidy.log \
			-o ../$(INSERTION_TITRE)/$$i $$i ; \
		if (( $$? == 1 )) ; \
			then \
				echo \\n\\n\*\*\* file $$i >> tidy_all.log ; \
				cat tidy.log >> tidy_all.log ; \
		fi ; \
		if (( $$? == 2 )) ; \
			then \
				echo tidy: \*\*\* Error: file $$i ; \
		fi ; \
	done ; \
	cd ..)
	$(VOIRCOMM)ls $(TIDY)/*.css > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp $(TIDY)/*.css $(INSERTION_TITRE); \
		true;
	$(VOIRCOMM)ls $(TIDY)/*.png > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp $(TIDY)/*.png $(INSERTION_TITRE); \
		true;
	$(VOIRCOMM)ls $(TIDY)/*.gif > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp $(TIDY)/*.gif $(INSERTION_TITRE); \
		true;
	$(VOIRCOMM)-cp $(TIDY)/*.css $(CECOURS)
	$(VOIRCOMM)ls $(TIDY)/*.png > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp $(TIDY)/*.png $(CECOURS); \
		true;
	$(VOIRCOMM)ls $(TIDY)/*.gif > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp $(TIDY)/*.gif $(CECOURS); \
		true;
	$(VOIRCOMM)ls $(TIDY)/${BASEHTML}*.html > /dev/null 2> /dev/null; \
		EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp $(TIDY)/${BASEHTML}*.html \
			$(CECOURS); \
		true;
	$(VOIRCOMM)-recode -d ISO-8859-15..HTML_4.0 \
		$(CECOURS)/${BASEHTML}*.html
	$(VOIRCOMM)ln -f -s ${BASEHTML}.html $(INSERTION_TITRE)/index.html
	$(VOIRCOMM)make -C $(CONFIGTEX4HT) TITRE=$(TITRE) \
			LONGUEUR_MENU=$(LONGUEUR_MENU) \
			ORIGINE=$(ABSDIRC)/$(INSERTION_TITRE) \
			INTERMEDIAIRE=$(ABSDIRC)/$(INTERMEDIAIRE) \
			FINAL=$(ABSDIRC)/$(CECOURS) \
			TOC=$(ABSDIRC)/tochtml.tex
	$(VOIRCOMM)-cp $(INSERTION_TITRE)/${BASEHTML}.css $(CECOURS)
	$(VOIRCOMM)-cp $(CONFIGTEX4HT)/*.css $(CECOURS)
	$(VOIRCOMM)-cp $(CONFIGTEX4HT)/*.html $(CECOURS)
	$(VOIRCOMM)ls $(FIGDIR)/*.png > /dev/null 2> /dev/null; EXIST=$$?; \
		[[ $$EXIST -eq 0 ]] && cp $(FIGDIR)/*.png $(CECOURS)/Figures; \
		true;
	$(VOIRCOMM)chmod -R go+r $(ENLIGNE)/*
	$(VOIRCOMM)find $(ENLIGNE) -type d -exec chmod go+x {} \;
	$(VOIRCOMM)touch lastHTML.date
	$(VOIRCOMM)make post-html
else
	$(VOIRCOMM)true
endif
else
	$(VOIRCOMM)true
endif

tout : pre-tout html chap chapens diapos post-tout

public: pre-public tout
ifneq ($(findstring $(REP),$(PUBLISHSLIDES)),)
	$(VOIRCOMM)mkdir -p $(PUBLICDIR)/Diapos
ifneq ($(findstring $(REP),$(PUBLISHPS)),)
	$(VOIRCOMM)-cp $(C)-diapos.ps $(PUBLICDIR)/Diapos
else
	$(VOIRCOMM)true
endif
ifneq ($(findstring $(REP),$(PUBLISHPDF)),)
	$(VOIRCOMM)-cp $(C)-diapos.pdf $(PUBLICDIR)/Diapos
else
	$(VOIRCOMM)true
endif
else
	$(VOIRCOMM)true
endif
ifneq ($(findstring $(REP),$(PUBLISHCHAP)),)
	$(VOIRCOMM)mkdir -p $(PUBLICDIR)/Chap
ifneq ($(findstring $(REP),$(PUBLISHPS)),)
	$(VOIRCOMM)-cp $(C)-chap.ps $(PUBLICDIR)/Chap
else
	$(VOIRCOMM)true
endif
ifneq ($(findstring $(REP),$(PUBLISHPDF)),)
	$(VOIRCOMM)-cp $(C)-chap.pdf $(PUBLICDIR)/Chap
else
	$(VOIRCOMM)true
endif
else
	$(VOIRCOMM)true
endif
ifneq ($(findstring $(REP),$(PUBLISHCHAPENS)),)
	$(VOIRCOMM)mkdir -p $(PUBLICDIR)/ChapEns
ifneq ($(findstring $(REP),$(PUBLISHPS)),)
	$(VOIRCOMM)-cp $(C)-chapens.ps $(PUBLICDIR)/ChapEns
else
	$(VOIRCOMM)true
endif
ifneq ($(findstring $(REP),$(PUBLISHPDF)),)
	$(VOIRCOMM)-cp $(C)-chapens.pdf $(PUBLICDIR)/ChapEns
else
	$(VOIRCOMM)true
endif
else
	$(VOIRCOMM)true
endif
ifneq ($(findstring $(REP),$(PUBLISHHTML)),)
	$(VOIRCOMM)mkdir -p $(PUBLICDIR)/EnLigne/Images
	$(VOIRCOMM)-cp -rH $(RESSOURCESDIR)/Images/* $(PUBLICDIR)/EnLigne/Images
	$(VOIRCOMM)mkdir -p $(PUBLICDIR)/EnLigne/$(DIRC)
	$(VOIRCOMM)[[ ! -e $(PUBLICDIR)/EnLigne/$(DIRC)/Images ]] && \
		(ln -s  ../../Images $(PUBLICDIR)/EnLigne/$(DIRC)/Images); \
		true
	$(VOIRCOMM)-(cd $(CECOURS); cp -r [!I]* $(PUBLICDIR)/EnLigne/$(DIRC); cd ../..)
else
	$(VOIRCOMM)true
endif
	$(VOIRCOMM)make post-public

cleanpublic: pre-cleanpublic
	$(VOIRCOMM)echo ERREUR : cette cible n\'est normalement jamais appelee

cleanall: pre-cleanall clean
	$(VOIRCOMM)rm -f *.ps *.pdf *.tgz *.toc lastCHAP.date lastCHAPENS.date \
		lastSLIDES.date lastHTML.date
	$(VOIRCOMM)rm -rf $(ENLIGNE)
	$(VOIRCOMM)[[ -d $(RESSOURCESDIR)/$(CONFIGTEX4HT) ]] && \
		make -C $(RESSOURCESDIR)/$(CONFIGTEX4HT) cleanall; true
	$(VOIRCOMM)find . -type l -exec rm -f {} \;

clean:	pre-clean lightclean
	$(VOIRCOMM)rm -f *.dvi
	$(VOIRCOMM) rm -f $(FIGDIR)/*.bak
	$(VOIRCOMM) rm -f $(FIGDIR)/[!_]*.eps $(FIGDIR)/[!_]*.png $(FIGDIR)/[!_]*.pdf


lightclean: pre-lightclean
	$(VOIRCOMM)-rm -f *.log *.aux *.bbl *.blg *~ *.bak *.out \
		*.idx *.ilg *.ind *.cmd *.gnd *.glg \
		*.4dx *.4ix *.5dx *.4gx *.5gx \
		*.lof *.los *.lii *.lqc  *.tmp toc_*.tex *.sec tochtml.tex \
		*.cb *.css *.idv *.lg *.xref *ppm *4ct *4tc *.fls \
		index.delme bib.delme
	$(VOIRCOMM)-rm -f *.html
	$(VOIRCOMM)find . -name "*~" -exec rm -f {} \;
	$(VOIRCOMM)rm -rf $(TIDY) $(INSERTION_TITRE) $(INTERMEDIAIRE)

%.eps: %.fig
	$(VOIRCOMM)fig2dev -L eps $< > $@

%.pdf: %.fig
	$(VOIRCOMM)fig2dev -L pdf $< > $@

%.eps: %.latexfig
	$(VOIRCOMM)(cd ./Figures ; ./transform_latexfig.sh $<)

%.eps: %.dia
	$(VOIRCOMM)dia -t eps $< -e $@

%.pdf: %.dia
	$(VOIRCOMM)dia -t pdf $< -e $@

%.eps: %.svg
	$(VOIRCOMM)inkscape --export-eps=$@ $<

%.pdf: %.svg
	$(VOIRCOMM)inkscape --export-pdf=$@ $<
