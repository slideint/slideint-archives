BASEDIR = $(CURDIR)/..

include ../Makefile-configure

SHELL   = /bin/bash
LATEX	= /usr/bin/pdflatex

BIBS	= $(wildcard *.bib)
FIGURES	= $(wildcard *.fig)

PUBLICDIR = ../Public

ADDSUFFTEX = $(addsuffix /*.tex, $(lstex))
LISTTEX := $(sort $(foreach lstex, $(LABSDIRC), $(ADDSUFFTEX)))
WILDCARDTEX = $(wildcard $(LISTTEX))
ADDSUFFTOC = $(addsuffix /toc_*.tex, $(lstoc))
LISTTOC := $(sort $(foreach lstoc, $(LABSDIRC), $(ADDSUFFTOC)))
WILDCARDTOC = $(wildcard $(LISTTOC))
ADDSUFFTOCHTML = $(addsuffix /tochtml.tex, $(lstochtml))
LISTTOCHTML := $(sort $(foreach lstochtml, $(LABSDIRC), $(ADDSUFFTOCHTML)))
WILDCARDTOCHTML = $(wildcard $(LISTTOCHTML))
SOURCES = $(filter-out $(WILDCARDTOC), $(WILDCARDTEX)) $(wildcard *.tex)
LASTCHAP= $(SOURCES) $(BIBS) $(FIGURES) lastCHAP.date
NEEDCHAP= $(word 1,$(shell ls -1 --sort=t $(LASTCHAP) 2> /dev/null))
LASTCHAPENS= $(SOURCES) $(BIBS) $(FIGURES)lastCHAPENS.date
NEEDCHAPENS= $(word 1,$(shell ls -1 --sort=t $(LASTCHAPENS) 2> /dev/null))
ifneq ($(VERBOSE),)
LATEXREDIRECT = 
else
LATEXREDIRECT = > /dev/null
VOIRCOMM = @
endif

include Makefile-configure

voirvar : pre-voirvar
	@echo Poly/Makefile : ADDSUFFTEX = $(ADDSUFFTEX)
	@echo Poly/Makefile : LISTTEX = $(LISTTEX)
	@echo Poly/Makefile : WILDCARDTEX = $(WILDCARDTEX)
	@echo Poly/Makefile : ADDSUFFTOC = $(ADDSUFFTOC)
	@echo Poly/Makefile : LISTTOC = $(LISTTOC)
	@echo Poly/Makefile : WILDCARDTOC = $(WILDCARDTOC)
	@echo Poly/Makefile : ADDSUFFTOCHTML = $(ADDSUFFTOCHTML)
	@echo Poly/Makefile : LISTTOCHTML = $(LISTTOCHTML)
	@echo Poly/Makefile : WILDCARDTOCHTML = $(WILDCARDTOCHTML)
	@echo Poly/Makefile : SOURCES = $(SOURCES)
	@echo Poly/Makefile : LASTCHAP = $(LASTCHAP)
	@echo Poly/Makefile : NEEDCHAP = $(NEEDCHAP)
	@echo Poly/Makefile : LASTCHAPENS = $(LASTCHAPENS)
	@echo Poly/Makefile : NEEDCHAPENS = $(NEEDCHAPENS)
	@echo Poly/Makefile : LATEXREDIRECT = $(LATEXREDIRECT)
	@echo Poly/Makefile : VOIRCOMM = $(VOIRCOMM)

all:	pre-all chap chapens post-all

tout:	pre-tout all post-tout

liens: pre-liens
	$(VOIRCOMM)[[ ! -e ConfigTex4ht ]] && \
		(ln -s  $(RESSOURCESDIR)/ConfigTex4ht .); true
	$(VOIRCOMM)[[ ! -e Images ]] && ln -s  $(RESSOURCESDIR)/Images .; true
	$(VOIRCOMM)[[ ! -e Makefile-configure ]] && \
		(ln -s  $(RESSOURCESDIR)/Makefile-configure .); true
	$(VOIRCOMM)[[ ! -e newslide.cls ]] && \
		(ln -s  $(RESSOURCESDIR)/newslide.cls .); true
	$(VOIRCOMM)[[ ! -e newslide.4ht ]] && \
		(ln -s  $(RESSOURCESDIR)/newslide.4ht .); true
	$(VOIRCOMM)[[ ! -e slideint.sty ]] && \
		(ln -s  $(RESSOURCESDIR)/slideint.sty .); true
	$(VOIRCOMM)[[ ! -e slideint.4ht ]] && \
		(ln -s  $(RESSOURCESDIR)/slideint.4ht .); true

%.eps: %.gif
	$(VOIRCOMM)[[ ! -e $@ ]] && convert $< $@; true

%.png: %.gif
	$(VOIRCOMM)[[ ! -e $@ ]] && convert $< $@; true

%.eps: %.jpg
	$(VOIRCOMM)[[ ! -e $@ ]] && convert $< $@; true

%.png: %.jpg
	$(VOIRCOMM)[[ ! -e $@ ]] && convert $< $@; true

%.eps: %.png
	$(VOIRCOMM)[[ ! -e $@ ]] && convert $< $@; true

%.eps: %.fig
	$(VOIRCOMM)fig2dev -L eps $< > $@

%.png: %.fig
	$(VOIRCOMM)fig2dev -L png $< > $@

%.eps: %.dia
	$(VOIRCOMM)dia -t eps $< -e $@

%.png: %.dia
	$(VOIRCOMM)dia -t png $< -e $@

%.eps: %.svg
	$(VOIRCOMM)inkscape --export-eps=$@ $<

%.png: %.svg
	$(VOIRCOMM)inkscape --export-png=$@ $<

chap : lightclean liens pre-chap
ifneq ($(findstring $(REP),$(GENERATECHAP)),)
ifneq ($(NEEDCHAP),lastCHAP.date)
ifneq ($(findstring $(REP),$(GENERATEPS) $(GENERATEPDF)),)
	$(VOIRCOMM)(cp debut.tex poly.tex; \
	 for d in $(POLY); \
	 do \
		export FNAME=$$(echo $$d| tr [:upper:] [:lower:]|cut -d/ -f 2);\
		[[ ! -e ../$$d/$$FNAME-chap.tex ]] && \
			(ln -s ../../Ressources/Common-tex/common-chap.tex \
				../$$d/$$FNAME-chap.tex); true; \
		echo "\\begin{papers}" >> poly.tex; \
		echo "\\emptyAtBeginDocument % évite les récursions infinies au \\begin{document}" >> poly.tex; \
		echo "\\importcombine{"$$FNAME"-chap}{../"$$d"/}" >> poly.tex; \
		echo "\\end{papers}" >> poly.tex; \
		echo "\\cleardoublepage" >> poly.tex; \
	 done; \
	 cat fin.tex >> poly.tex)
	$(VOIRCOMM)$(LATEX) -file-line-error poly.tex $(LATEXREDIRECT)
# mettre un appel à bibtex sur chaque présentation pour bib locale
	$(VOIRCOMM)if grep -q '\\bibliography{' $(SOURCES) poly.tex ; \
		then bibtex poly; true \
		else true; \
	fi
	$(VOIRCOMM)-make other_bibtex BIBTEXFILE=poly
	$(VOIRCOMM)$(LATEX) -file-line-error poly.tex $(LATEXREDIRECT)
	$(VOIRCOMM)-make other_makeindex MAKEINDEXFILE=poly
	$(VOIRCOMM)if grep -q '\\slideprintindex' $(SOURCES) ; \
		then makeindex -L -s index.isty -t index.ilg poly \
			$(LATEXREDIRECT); true \
		else true; \
	fi
	$(VOIRCOMM)[[ -a poly.bbl || -a poly.ilg ]] && \
		($(LATEX) -file-line-error poly.tex \
			$(LATEXREDIRECT)); true
	$(VOIRCOMM)for i in toc_*.tex ; \
	do\
		cp $$i tmp.tex ; \
		uniq tmp.tex > $$i ; \
		rm tmp.tex ; \
	done
	$(VOIRCOMM)$(LATEX) -file-line-error poly.tex $(LATEXREDIRECT)
else
	$(VOIRCOMM)true
endif
ifneq ($(findstring $(REP),$(GENERATEPS)),)
ifeq ($(NBPERPAGE),$(ONEPERPAGE))
	$(VOIRCOMM) NOW, USE OF PDFLATEX FOR POLY => NO MORE PostScript VERSION
else
	$(VOIRCOMM) NOW, USE OF PDFLATEX FOR POLY => NO MORE PostScript VERSION
endif
endif
ifneq ($(findstring $(REP),$(GENERATEPDF)),)
ifeq ($(PS2PDF_DVIPDFM),$(DVIPDFM))
ifeq ($(NBPERPAGE),$(ONEPERPAGE))
#	$(VOIRCOMM)dvipdfm -p a4 -z9 -o poly.pdf poly.dvi
else
	$(VOIRCOMM)pdfjoin --no-tidy poly.pdf '1-2'
	$(VOIRCOMM)mv poly-1-2-joined.pdf poly-debut.pdf
	$(VOIRCOMM)pdfjoin --no-tidy poly.pdf '3-'
	$(VOIRCOMM)mv poly-3--joined.pdf poly-fin.pdf
	$(VOIRCOMM)pdfnup --nup '2x1' --papersize '{21cm,29.7cm}' --landscape --suffix '2pp' poly-fin.pdf
	$(VOIRCOMM)pdfjoin --no-tidy poly-debut.pdf '-' poly-fin-2pp.pdf '-'
	$(VOIRCOMM)rm -f poly-fin.pdf poly-debut.pdf poly-fin-2pp.pdf
	$(VOIRCOMM)cp poly.pdf poly-1pp.pdf
	$(VOIRCOMM)mv poly-fin-2pp-joined.pdf poly.pdf
endif
else
ifeq ($(NBPERPAGE),$(ONEPERPAGE))
	$(VOIRCOMM) NOW, USE OF PDFLATEX FOR POLY => NO MORE PSP2DF
else
	$(VOIRCOMM) NOW, USE OF PDFLATEX FOR POLY => NO MORE PSP2DF
endif
	$(VOIRCOMM) NOW, USE OF PDFLATEX FOR POLY => NO MORE PSP2DF
endif
endif
	$(VOIRCOMM)touch lastCHAP.date
	$(VOIRCOMM)make post-chap
else
	$(VOIRCOMM)true
endif
else
	$(VOIRCOMM)true
endif

chapens : lightclean liens pre-chapens $(FIGURES:%.fig=%.pdf)
ifneq ($(findstring $(REP),$(GENERATECHAPENS)),)
ifneq ($(NEEDCHAPENS),lastCHAPENS.date)
ifneq ($(findstring $(REP),$(GENERATEPS) $(GENERATEPDF)),)
	$(VOIRCOMM)(cp debut-ens.tex poly-ens.tex; \
	 for d in $(POLYENS); \
	 do \
		export FNAME=$$(echo $$d| tr [:upper:] [:lower:]|cut -d/ -f 2);\
		[[ ! -e ../$$d/$$FNAME-chapens.tex ]] && \
			(ln -s ../../Ressources/Common-tex/common-chapens.tex \
				../$$d/$$FNAME-chapens.tex); true; \
		echo "\\begin{papers}" >> poly-ens.tex; \
		echo "\\emptyAtBeginDocument % évite les récursions infinies au \\begin{document}" >> poly-ens.tex; \
		echo "\\importcombine{"$$FNAME"-chapens}{../"$$d"/}" >> poly-ens.tex; \
		echo "\\end{papers}" >> poly-ens.tex; \
		echo "\\cleardoublepage" >> poly-ens.tex; \
	 done; \
	 cat fin.tex >> poly-ens.tex)
	$(VOIRCOMM)$(LATEX) -file-line-error poly-ens.tex $(LATEXREDIRECT)
# mettre un appel à bibtex sur chaque présentation pour bib locale
	$(VOIRCOMM)-make other_bibtex BIBTEXFILE=poly-ens
	$(VOIRCOMM)if grep -q '\\bibliography{' $(SOURCES) poly-ens.tex ; \
		then bibtex poly-ens; true \
		else true; \
	fi
	$(VOIRCOMM)$(LATEX) -file-line-error poly-ens.tex $(LATEXREDIRECT)
	$(VOIRCOMM)-make other_makeindex MAKEINDEXFILE=poly-ens
	$(VOIRCOMM)if grep -q '\\slideprintindex' $(SOURCES) ; \
		then makeindex -L -s index.isty -t index.ilg poly-ens; true \
		else true; \
	fi
	$(VOIRCOMM)[[ -a poly-ens.bbl || -a poly-ens.ilg ]] && \
		($(LATEX) -file-line-error poly-ens.tex $(LATEXREDIRECT)); true
	$(VOIRCOMM)@for i in toc_*.tex ; \
	do\
		cp $$i tmp.tex ; \
		uniq tmp.tex > $$i ; \
		rm tmp.tex ; \
	done
	$(VOIRCOMM)$(LATEX) -file-line-error poly-ens.tex $(LATEXREDIRECT)
else
	$(VOIRCOMM)true
endif
ifneq ($(findstring $(REP),$(GENERATEPS)),)
ifeq ($(NBPERPAGE),$(ONEPERPAGE))
	$(VOIRCOMM) NOW, USE OF PDFLATEX FOR POLY => NO MORE PostScript VERSION
else
	$(VOIRCOMM) NOW, USE OF PDFLATEX FOR POLY => NO MORE PostScript VERSION
endif
endif
ifneq ($(findstring $(REP),$(GENERATEPS) $(GENERATEPDF)),)
ifeq ($(PS2PDF_DVIPDFM),$(DVIPDFM))
ifeq ($(NBPERPAGE),$(ONEPERPAGE))
#	$(VOIRCOMM)dvipdfm -p a4 -z9 -o poly-ens.pdf poly-ens.dvi
else
	$(VOIRCOMM)pdfjoin --no-tidy poly-ens.pdf '1-2'
	$(VOIRCOMM)mv poly-ens-1-2-joined.pdf poly-ens-debut.pdf
	$(VOIRCOMM)pdfjoin --no-tidy poly-ens.pdf '3-'
	$(VOIRCOMM)mv poly-ens-3--joined.pdf poly-ens-fin.pdf
	$(VOIRCOMM)pdfnup --nup '2x1' --papersize '{21cm,29.7cm}' --landscape --suffix '2pp' poly-ens-fin.pdf
	$(VOIRCOMM)pdfjoin --no-tidy poly-ens-debut.pdf '-' poly-ens-fin-2pp.pdf '-'
	$(VOIRCOMM)rm -f poly-ens-fin.pdf poly-ens-debut.pdf poly-ens-fin-2pp.pdf
	$(VOIRCOMM)cp poly-ens.pdf poly-ens-1pp.pdf
	$(VOIRCOMM)mv poly-ens-fin-2pp-joined.pdf poly-ens.pdf
endif
else
ifeq ($(NBPERPAGE),$(ONEPERPAGE))
	$(VOIRCOMM) NOW, USE OF PDFLATEX FOR POLY => NO MORE PSP2DF
else
#else
#	$(VOIRCOMM) NOW, USE OF PDFLATEX FOR POLY => NO MORE PSP2DF
endif
	$(VOIRCOMM) NOW, USE OF PDFLATEX FOR POLY => NO MORE PS2PDF
endif
endif
	$(VOIRCOMM)touch lastCHAPENS.date
	$(VOIRCOMM)make post-chapens
else
	$(VOIRCOMM)true
endif
else
	$(VOIRCOMM)true
endif

diapos:
# rien pour le polycopié

html:
# rien pour le polycopié

public: pre-public tout
ifneq ($(findstring $(REP),$(PUBLISHCHAP)),)
	$(VOIRCOMM)mkdir -p $(PUBLICDIR)/Poly
ifneq ($(findstring $(REP),$(PUBLISHPS)),)
	$(VOIRCOMM)cp poly.ps $(PUBLICDIR)/Poly
else
	$(VOIRCOMM)true
endif
ifneq ($(findstring $(REP),$(PUBLISHPDF)),)
	$(VOIRCOMM)cp poly-1pp.pdf $(PUBLICDIR)/Poly
	$(VOIRCOMM)cp poly.pdf $(PUBLICDIR)/Poly
else
	$(VOIRCOMM)true
endif
else
	$(VOIRCOMM)true
endif
ifneq ($(findstring $(REP),$(PUBLISHCHAPENS)),)
	$(VOIRCOMM)mkdir -p $(PUBLICDIR)/PolyEns
ifneq ($(findstring $(REP),$(PUBLISHPS)),)
	$(VOIRCOMM)cp poly-ens.ps $(PUBLICDIR)/PolyEns
else
	$(VOIRCOMM)true
endif
ifneq ($(findstring $(REP),$(PUBLISHPDF)),)
	$(VOIRCOMM)cp poly-ens-1pp.pdf $(PUBLICDIR)/PolyEns
	$(VOIRCOMM)cp poly-ens.pdf $(PUBLICDIR)/PolyEns
else
	$(VOIRCOMM)true
endif
else
	$(VOIRCOMM)true
endif
	$(VOIRCOMM)make post-public

cleanpublic: pre-cleanpublic
	$(VOIRCOMM)echo ERREUR cible cleanpublic : cette cible n\'est \
		normalement jamais appelee

cleanall: pre-cleanall clean
	$(VOIRCOMM)rm -f *.ps *.pdf *.tgz *.toc *.lqc lastCHAP.date lastCHAPENS.date

clean:	pre-clean
	$(VOIRCOMM)rm -f *.dvi

lightclean: pre-lightclean
	$(VOIRCOMM)rm -f *.log *.aux *.bbl *.blg *~ *.bak *.out \
		[!_]*.eps [!_]*.png *.idx *.tmp *.lof *.lot toc_*.tex \
		*.ilg *.ind poly*.tex
