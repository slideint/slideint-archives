To build the image of the container:
$ docker build -t tsp/slideint-slim .

To run the container:
$ cd to_a_directory_using_slideint
$ docker run -v $PWD:/data -it tsp/slideint-slim
and in the container:
$ cd data
$ slideint
