To build the image of the container:
$ docker build -t tsp/slideint .

To run the container:
$ cd to_a_directory_using_slideint
$ docker run -v $PWD:/home/$USER -it tsp/slideint
and in the container:
$ cd data
$ slideint
