#!/bin/bash

if [ $# -ne 2 ]
then
    echo "Usage $0 version repository"
    echo "where  version is in the form yearmonthday"
    echo "and repository is the directory in which bin packages will be stored"
    exit 1
fi
# $1 is the name of the version, in the form yearmonthday
# $2 is the location of the apt repository where to deploy the package
echo '$1' = $1
echo '$2' = $2
# even if the directory debian is not part of the package, export it
rm -rf /tmp/slideint-*
cd ../..
svn export trunk /tmp/slideint-debian-$1
svn export web/man-slideint /tmp/slideint-debian-$1/Man
svn export web/tutorials-slideint /tmp/slideint-debian-$1/tutorials
cd /tmp/slideint-debian-$1
mv debian-slideint debian
mkdir examples-slideint/fulfledged
mv examples-slideint/01Presentation-ISO examples-slideint/fulfledged/.
mv examples-slideint/01Presentation-UTF8 examples-slideint/fulfledged/.
mv examples-slideint/PresentationSlideBeamer-UTF8 examples-slideint/fulfledged/.
mv examples-slideint/02Course-ISO examples-slideint/fulfledged/.
mv examples-slideint/02Course-UTF8 examples-slideint/fulfledged/.
mv tutorials examples-slideint/.
mkdir -p texmf/tex/latex/slideint
find slideint -name "*.cls" -exec mv {} texmf/tex/latex/slideint \;
find slideint -name "*.sty" -exec mv {} texmf/tex/latex/slideint \;
find slideint -name "*.4ht" -exec mv {} texmf/tex/latex/slideint \;
cd /tmp/slideint-debian-$1/debian
# fill in the new version change log
dch -v $1
cp changelog /tmp
cd ..
# -us: non signed source package; -uc: non signed file .changes
dpkg-buildpackage -rfakeroot
#rm -f $2/binary/slideint_*_all.deb
cp ../slideint_$1_all.deb $2/binary/
#rm -f $2/sources/slideint_*
cp ../slideint_$1.tar.gz ../slideint_$1.dsc $2/sources/
cd $2
dpkg-scanpackages binary /dev/null | gzip -9c > binary/Packages.gz
chmod a+r binary/*
dpkg-scansources sources /dev/null | gzip -9c > sources/Sources.gz
chmod a+r sources/*
#rm -rf /tmp/slideint*
