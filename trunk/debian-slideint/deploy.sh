#!/bin/bash

# memorise the current working directory
oldrep=${PWD}
echo Give the version name of the package \(e.g. YearMonthDay\)
read x
# in the directory debian of slideint
cd ${HOMESLIDEINT}/trunk/debian-slideint
# launch the script that creates the package
./slideint_package.sh ${x} ${HOMESLIDEINT}/web/repositories/apt-repo
# return to the directory debian of slideint
cd ${HOMESLIDEINT}/trunk/debian-slideint
cp ${HOMESLIDEINT}/web/repositories/apt-repo/binary/slideint_${x}_all.deb /tmp
# in directory /tmp
cd /tmp
cp slideint*.changes ${HOMESLIDEINT}/web/repositories/apt-repo/sources/
cp slideint*.dsc ${HOMESLIDEINT}/web/repositories/apt-repo/binary/
find ${HOMESLIDEINT}/web/repositories -type d -exec chmod a+rx {} \;
find ${HOMESLIDEINT}/web/repositories -type f -exec chmod a+r {} \;
echo Update and deployment done.
ls -lR ${HOMESLIDEINT}/web/repositories
mv ${HOMESLIDEINT}/trunk/debian-slideint/changelog ${HOMESLIDEINT}/trunk/debian-slideint/changelog.old
mv /tmp/changelog ${HOMESLIDEINT}/trunk/debian-slideint
echo END
cd ${oldrep}
