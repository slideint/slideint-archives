#!/bin/sh

cd ..
find web/ -type d -exec chmod a+rx {} \;
find web/ -type f -exec chmod a+r {} \;
find web/ -type d -exec chmod g+w {} \;
rsync -r -e ssh -z --delete -p --exclude publish.sh web/* $SLIDEINTUSER@fusionforge.int-evry.fr:/slideint/htdocs/.
cd web
