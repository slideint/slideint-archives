Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20160713
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 e07af786b454f3010888e5e0b1083bf6caa8a89a 30368547 slideint_20160713.tar.gz
Checksums-Sha256:
 35ac3736b41b2e5469272c64a65a3b57e16c6f3652bfa7faea7f9ab1f94be78f 30368547 slideint_20160713.tar.gz
Files:
 79ad72ef6a6759e4b283623a668780fe 30368547 slideint_20160713.tar.gz
