Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20171120
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 f0040eb3a668828c0a3f383972eb2427fe427cc5 30229601 slideint_20171120.tar.gz
Checksums-Sha256:
 48475193c39a1b77b8198da530978c5a8cbe8bf59e4e8fc2774b3a7e2dc3acae 30229601 slideint_20171120.tar.gz
Files:
 d82ed4905b94faf118f2caf7df2a7d31 30229601 slideint_20171120.tar.gz
