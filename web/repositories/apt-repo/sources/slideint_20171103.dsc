Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20171103
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 6c88e2cc9dba5d57b9ff0804f480ddbf2f26ae9d 30225668 slideint_20171103.tar.gz
Checksums-Sha256:
 eb99163a477f7a94b1b75714145b39915194a1a98840cad6da0623c1f07d7744 30225668 slideint_20171103.tar.gz
Files:
 df50f2a797a79e6dff7486fcee54716b 30225668 slideint_20171103.tar.gz
