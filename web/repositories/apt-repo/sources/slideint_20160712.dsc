Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20160712
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 8f8426c4330b7e3a82cb1e63bad1b60ded894c06 30346028 slideint_20160712.tar.gz
Checksums-Sha256:
 b67ebb7c1d929deaccf7f2e6c1cb9ea3c238d55621369ce25824e173bf2666dd 30346028 slideint_20160712.tar.gz
Files:
 fae1c335347404829aeb8d213f9ae93b 30346028 slideint_20160712.tar.gz
