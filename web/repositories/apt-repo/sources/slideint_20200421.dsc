Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20200421
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 b2b9540900994ca7fb1e3e2f07fbff408ad8c3a7 30171674 slideint_20200421.tar.gz
Checksums-Sha256:
 acf166b57e694fa27252cae8f5810d98a9e6be4c32f3d16033194a458bc3d5a7 30171674 slideint_20200421.tar.gz
Files:
 e8bc799be155e73e2e87800843b5786d 30171674 slideint_20200421.tar.gz
