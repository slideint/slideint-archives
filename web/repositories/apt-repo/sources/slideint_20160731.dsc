Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20160731
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 f63a5fe583856f700b3c3b0300f0497fa66c1c14 30235649 slideint_20160731.tar.gz
Checksums-Sha256:
 015b744bd90fd608a436655e2283c6aae0bf1815ea6a780be0f530947d4efa2b 30235649 slideint_20160731.tar.gz
Files:
 4ffc5c157e6e56ab8c6169c1a972ad2d 30235649 slideint_20160731.tar.gz
