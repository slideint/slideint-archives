Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20211111
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 d5dd7bdf9b95e56ba67609125983d4efc401b08d 30174996 slideint_20211111.tar.gz
Checksums-Sha256:
 5a1bf8a8a582413e07b3f2fe555fcd47dc1fe8655743618f40f4aebb50e0918f 30174996 slideint_20211111.tar.gz
Files:
 a05088157120eef5cfafe4cc1c539f37 30174996 slideint_20211111.tar.gz
