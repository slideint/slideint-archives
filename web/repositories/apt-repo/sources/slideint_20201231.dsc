Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20201231
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 eaa875951dc59f4cf5e3a921b4a6de92906da00a 30173960 slideint_20201231.tar.gz
Checksums-Sha256:
 c8f9ca8b54c00807cad77f0edad7e441abc0beef13b8be13c9efd1212559f9cc 30173960 slideint_20201231.tar.gz
Files:
 f591bc06f2a73ea7213bfd2db90a9443 30173960 slideint_20201231.tar.gz
