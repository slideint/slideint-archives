Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20171117
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 10f64b3cca79d5d3dd62ab1c274fc6e19fc22ea2 30226845 slideint_20171117.tar.gz
Checksums-Sha256:
 82378c669858a49db098853d06bbc5275df8924fb02f6782cb58ad7cefdb945e 30226845 slideint_20171117.tar.gz
Files:
 90a5bfe1d7ea642f4a072668cdb6f499 30226845 slideint_20171117.tar.gz
