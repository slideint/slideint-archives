Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20160711
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 c596b0a6d0e7992122e08cb9b6a88232e57fff58 30385970 slideint_20160711.tar.gz
Checksums-Sha256:
 b264ef0b84c81da049b948f7a085c97029a6f4d7c1b484f2d141625c1851ee95 30385970 slideint_20160711.tar.gz
Files:
 2794d22e50184b5932b1d41981d4ebbe 30385970 slideint_20160711.tar.gz
