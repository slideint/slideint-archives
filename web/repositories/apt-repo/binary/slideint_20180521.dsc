Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20180521
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 0a6d027cc76d3e5491fb6db7cddde9b481934005 30169826 slideint_20180521.tar.gz
Checksums-Sha256:
 eeffbc511a88f31a7050b58c9d09be50d575105a1b62032288e5747965b112e8 30169826 slideint_20180521.tar.gz
Files:
 04d11ac57ca097dd79f037559a990c33 30169826 slideint_20180521.tar.gz
