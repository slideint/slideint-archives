Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20160726
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 4b25fa20f8106a92e678c2398c12e5ff5fb5afdd 30229757 slideint_20160726.tar.gz
Checksums-Sha256:
 bc03ffb4fc9525fe5c1b47df5f8651186deadd68372521352626281e6a46dd10 30229757 slideint_20160726.tar.gz
Files:
 97b4b400c7519aaa7e004abd3cef19ae 30229757 slideint_20160726.tar.gz
