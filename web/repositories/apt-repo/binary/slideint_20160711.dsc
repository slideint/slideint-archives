Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20160711
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 7d7ab3e89216e2bfb62e4b6080b0cad1c5904273 30353297 slideint_20160711.tar.gz
Checksums-Sha256:
 371f8cd97b110dc4486e378726e3793f2ee840085a43a19181d2025c43ea6918 30353297 slideint_20160711.tar.gz
Files:
 55de80367631e6b000c5ccaa0aa0fb5c 30353297 slideint_20160711.tar.gz
