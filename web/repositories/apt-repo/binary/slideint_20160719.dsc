Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20160719
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 4636b5eaa0218a251ee250c9656507ac7d865ae9 30232848 slideint_20160719.tar.gz
Checksums-Sha256:
 1a4473456657124bf916f7ca80ac339a4f98fea4c789ffbaaa2526a17a321a98 30232848 slideint_20160719.tar.gz
Files:
 c72b0d8a455c8c777b1e7752714f433b 30232848 slideint_20160719.tar.gz
