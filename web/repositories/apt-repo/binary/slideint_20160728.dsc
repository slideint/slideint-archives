Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20160728
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 d407a163fb68014777d867e5b498952f5843a4b3 30235721 slideint_20160728.tar.gz
Checksums-Sha256:
 1aa91beef2048f52378eac524c09a9e1bc21ecba83f6cf5c8f5b3477be9c418e 30235721 slideint_20160728.tar.gz
Files:
 f08d6ee2030b515007cb31cd4c58b19b 30235721 slideint_20160728.tar.gz
