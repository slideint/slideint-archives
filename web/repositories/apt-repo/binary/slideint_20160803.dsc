Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20160803
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 6f777788885368669a86c68221d9daff758ae8a8 30235351 slideint_20160803.tar.gz
Checksums-Sha256:
 ec28882bfdf0cd469e0cc41e7f0f7c1adf5672b3d2492ad87b18aebb260c34b2 30235351 slideint_20160803.tar.gz
Files:
 50698ca1abf589daffa0a77111f40dcb 30235351 slideint_20160803.tar.gz
