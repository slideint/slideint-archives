Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20160630
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 1e04524c80e0f1a76fce53132a7daa951b215d3e 30385189 slideint_20160630.tar.gz
Checksums-Sha256:
 dbd19f42628261ac1ccfed7742c9ae4b9638dfc150e484971e2154a4a08aed1f 30385189 slideint_20160630.tar.gz
Files:
 28ca8e7eda38a17938e8f369d82da77a 30385189 slideint_20160630.tar.gz
