Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20170617-2
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 e6962ef771dda3ae6d1f1b102c6ed404225fad29 30226162 slideint_20170617-2.tar.gz
Checksums-Sha256:
 a0e57d67e02960e954fceb4292ebabe72bed2d0634ab0e7b549b803643bcf159 30226162 slideint_20170617-2.tar.gz
Files:
 d0db4325cb0339679fe87b3e6565253c 30226162 slideint_20170617-2.tar.gz
