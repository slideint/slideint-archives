Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20170617
Maintainer: Denis Conan <denis.conan@it-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 89ebb7493ce77f05e7590646ac21a248beb7daa5 30223974 slideint_20170617.tar.gz
Checksums-Sha256:
 59158d75f50f2595731cf36742b83c75ce4f5bc8f1e10fcc4e2e7e68b9a8048a 30223974 slideint_20170617.tar.gz
Files:
 239137b4f330bedaf0a1df9532239c7e 30223974 slideint_20170617.tar.gz
